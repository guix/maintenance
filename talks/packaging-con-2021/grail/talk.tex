% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

% Make sure URLs are broken on hyphens.
% See <https://tex.stackexchange.com/questions/3033/forcing-linebreaks-in-url>.
\RequirePackage[hyphens]{url}

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}

% Nice sans-serif font.
\usepackage[sfdefault,lining]{FiraSans} %% option 'sfdefault' activates Fira Sans as the default text font
\renewcommand*\oldstylenums[1]{{\firaoldstyle #1}}

% Nice monospace font.
\usepackage{inconsolata}
%% \renewcommand*\familydefault{\ttdefault} %% Only if the base font of the document is to be typewriter style
\usepackage[T1]{fontenc}

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}

\usecolortheme{seagull}         % white on black
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for '\Verb'
\usepackage{xifthen}            % for '\isempty'

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\definecolor{rescienceyellow}{RGB}{254,246,91}

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{The Packaging Grail}

\author{Ludovic Courtès}
\date{10 November 2021}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}

\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center), opacity=.3, inner sep=10mm] {
      % https://thumbs.dreamstime.com/z/parcel-illustration-drawing-engraving-ink-line-art-vector-what-made-pencil-paper-then-was-digitalized-143335396.jpg
      \includegraphics[width=.9\textwidth]{images/parcel}
    };
    \node [at=(current page.center), fill=guixorange2, opacity=.4,
      text width=1.3\textwidth, text height=\textheight] {
    };
    \node [at=(current page.center), fill=black, opacity=.15,
      text width=1.3\textwidth, text height=\textheight] {};
  \end{tikzpicture}

  \vspace{35mm}
  \Huge{\textbf{The Packaging Grail}}
  \\[15mm]
  \large{Ludovic Courtès}
  \\[2mm]
  \textbf{PackagingCon \oldstylenums{2021}}

  \vfill{}
  \hfill{\includegraphics[width=0.2\paperwidth]{images/inria-white-2019}}
  \vspace{15mm}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 1: Guix's grail
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[fragile]
  \begin{tikzpicture}[overlay]
    \node(logo) [at=(current page.center), inner sep=0pt]
      {\includegraphics[width=.8\textwidth]{images/Guix-horizontal-print}};
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \begin{tikzpicture}[overlay]
    \node<1> at (current page.center) [anchor=center,
           inner sep=0mm, shape=star,
           fill=guixorange1, text=white] {
      \LARGE{\textbf{Free!}}
    };
    \node<2> at (current page.center) [anchor=center,
           inner sep=0mm, shape=star,
           fill=guixorange2, text=white] {
      \LARGE{\textbf{Universal!}}
    };
    \node<3> at (current page.center) [anchor=center,
           inner sep=0mm, shape=star,
           fill=guixred1, text=white] {
      \LARGE{\textbf{Transparent!}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center)] {
      \includegraphics[width=.7\paperwidth]{images/reproducible-builds}
    };
    \node [at=(current page.south), anchor=south, text=guixgrey] {
      \url{https://reproducible-builds.org}
    };
  \end{tikzpicture}
\end{frame}

%% \begin{frame}[fragile]
%%   \vspace{2.5cm}
%%   \begin{tikzpicture}[remember picture, overlay]
%%     \node [at=(current page.center), inner sep=0pt, rotate=4,
%%     drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
%%     {\includegraphics[width=0.9\paperwidth]{images/snap-crypto-miner}};
%%     \node<1> [at=(current page.south), anchor=south, color=guixgrey,
%%       fill=white, opacity=.5, text opacity=1]
%%     {\url{https://github.com/canonical-websites/snapcraft.io/issues/651}};
%%   \end{tikzpicture}
%% \end{frame}


\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[fragile]
  \begin{semiverbatim}
$ \alert{guix challenge} \\
    --substitute-urls="https://ci.guix.gnu.org https://example.org"
\uncover<2->{\alert{/gnu/store/\dots{}-openssl-1.0.2d contents differ}:
  local hash: 0725l22\dots{}
  http://ci.guix.gnu.org/\dots{}-openssl-1.0.2d: 0725l22\dots{}
  http://example.org/\dots{}-openssl-1.0.2d: 1zy4fma\dots{}
\alert{/gnu/store/\dots{}-git-2.5.0 contents differ}:
  local hash: 00p3bmr\dots{}
  http://ci.guix.gnu.org/\dots{}-git-2.5.0: 069nb85\dots{}
  http://example.org/\dots{}-git-2.5.0: 0mdqa9w\dots{}
\alert{/gnu/store/\dots{}-pius-2.1.1 contents differ}:
  local hash: 0k4v3m9\dots{}
  http://ci.guix.gnu.org/\dots{}-pius-2.1.1: 0k4v3m9\dots{}
  http://example.org/\dots{}-pius-2.1.1: 1cy25x1\dots{}}
  \end{semiverbatim}
\end{frame}
\setbeamercolor{normal text}{bg=black}

\setbeamercolor{normal text}{bg=guixdarkgrey}


\begin{frame}[plain]
  \LARGE{
    $\texttt{emacs} = f(\texttt{gtk+}, \texttt{gcc}, \texttt{make}, \texttt{coreutils})$
    \\[1.1cm]
    \uncover<2->{$\texttt{gtk+} = g(\texttt{glib}, \texttt{gcc}, \texttt{make}, \texttt{coreutils})$}
    \\[1.1cm]
    \uncover<3->{$\texttt{gcc} = h(\texttt{make}, \texttt{coreutils}, \texttt{gcc}_0)$}
    \\[1.1cm]
    \uncover<3->{\textrm{...}}
  }

  \uncover<1>{\large{where $f =$ \texttt{./configure \&\& make \&\& make install}}}

  %% \begin{tikzpicture}[overlay]
  %%   \node<4->[fill=guixorange1, text=black, text opacity=1, opacity=.7,
  %%         rounded corners=2mm, inner sep=5mm] at (5, 1) {
  %%           \textbf{\Large{the complete DAG is captured}}
  %%         };
  %% \end{tikzpicture}
\end{frame}
%% \begin{frame}[fragile]
%%   \begin{tikzpicture}[overlay]
%%     \node [at=(current page.north west), anchor=north west,
%%       outer sep=4mm, text=white, text width=13mm]{
%%       \texttt{configure},
%%       \texttt{src/hello.c},
%%       GCC,\\
%%       Binutils,
%%       etc.
%%     };
%%     \node [at=(current page.center), outer sep=3mm, font=\rmfamily]{
%%       {\fontfamily{roman}\fontsize{45}{45}{$f(x,y,z)$}}
%%     };
%%   \end{tikzpicture}
%% \end{frame}
%% \setbeamercolor{normal text}{bg=black}

\begin{frame}[fragile]
  %% \frametitle{Bit-Reproducible Builds$^*$}
  %% \framesubtitle{$^*$ almost!}

  \begin{semiverbatim}
    \Large{
\$ guix build hello
\uncover<2->{/gnu/store/\tikz[baseline]{\node[anchor=base](nixhash){\alert<2>{h2g4sf72\textrm{...}}};}-hello-2.10}

\uncover<3->{\$ \alert<3>{guix gc --references /gnu/store/\textrm{...}-hello-2.10}
/gnu/store/\textrm{...}-glibc-2.29
/gnu/store/\textrm{...}-gcc-7.4.0-lib
/gnu/store/\textrm{...}-hello-2.10
}}
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<1>(labelnixhash) [fill=white, text=black, inner sep=0.5cm,
       rounded corners] at (current page.center) {%
      \Large{\textbf{isolated build}: chroot, separate name spaces, etc.}
    };

    \node<2>(labelnixhash) [fill=white, text=black] at (4cm, 2cm) {%
      hash of \textbf{all} the dependencies};
    \path[->]<2>(labelnixhash.north) edge [bend left, in=180, out=-45] (nixhash.south);

    \draw<4-> (-10pt, 105pt) [very thick, color=guixorange2, rounded corners=8pt]
      arc (10:-50:-50pt and 110pt);
    \node<4->[fill=white, text=black, text opacity=1, opacity=.7,
          rounded corners=2mm, inner sep=5mm]
      at (7, 2) {\textbf{\Large{(nearly) bit-identical for everyone}}};
  \end{tikzpicture}

\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center)] {
      \includegraphics[width=.9\paperwidth]{images/bootstrappable}
    };
    \node [at=(current page.south), anchor=south, text=guixgrey] {
      \url{https://bootstrappable.org}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{fg=black,bg=white}
\begin{frame}[fragile]
  \vspace{2.5cm}
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt,
    drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
    {\includegraphics[height=0.9\paperheight]{images/reflections-on-trusting-trust}};
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \vspace{2.5cm}
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt, rotate=8,
    drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
    {\includegraphics[width=0.9\paperwidth]{images/strawhorse-attack}};
    \node<1> [at=(current page.south), anchor=south, color=guixgrey,
      fill=white, opacity=.5, text opacity=1]
    {\url{https://theintercept.com/2015/03/10/ispy-cia-campaign-steal-apples-secrets/}};
    % https://theintercept.com/document/2015/03/10/strawhorse-attacking-macos-ios-software-development-kit/

    \node<2-> [at=(current page.center), inner sep=0pt, rotate=-4,
    drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
    {\includegraphics[width=0.8\paperwidth]{images/rusting-trust}};
    \node<2> [at=(current page.south), anchor=south, color=guixgrey,
      fill=white, opacity=.5, text opacity=1]
    {\url{https://manishearth.github.io/blog/2016/12/02/reflections-on-rusting-trust/}};

    \node<3-> [at=(current page.center), inner sep=0pt, rotate=2,
    drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
    {\includegraphics[width=0.9\paperwidth]{images/deniable-compiler-backdoors}};
    \node<3> [at=(current page.south), anchor=south, color=guixgrey,
      fill=white, opacity=.5, text opacity=1]
    {\url{https://www.alchemistowl.org/pocorgtfo/pocorgtfo08.pdf}};
    % TODO: SolarWinds
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[height=\paperheight]{images/bootstrap-graph}};
    \node<2-> [at=(current page.center), anchor=north, inner sep=20pt, text=guixgrey]
      {\Large{\textbf{250 MiB of binary blobs}}};
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[height=\paperheight]{images/bootstrap-graph-reduced}};
    \node<2-> [at=(current page.center), fill=guixorange1, rounded corners=10pt,
               inner sep=10pt, opacity=.8, text opacity=1]
      {\Large{\textbf{250 MiB $\rightarrow$ 130 MiB of binary blobs}}};
    \node<2-> [at=(current page.south), anchor=south,
               inner sep=2mm, outer sep=3mm, rounded corners,
               fill=white, opacity=.7, text opacity=1, text=black]
      {\url{https://guix.gnu.org/en/blog/2019/guix-reduces-bootstrap-seed-by-50/}};
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[height=.8\paperheight]{images/bootstrap-graph-further-reduced}};
    \node<2-> [at=(current page.center), fill=guixorange1, rounded corners=10pt,
               inner sep=10pt, opacity=.8, text opacity=1]
      {\Large{\textbf{130 MiB $\rightarrow$ 60 MiB of binary blobs}}};
    \node<2-> [at=(current page.south), anchor=south,
               inner sep=2mm, outer sep=3mm, rounded corners,
               fill=white, opacity=.7, text opacity=1, text=black]
      {\url{https://guix.gnu.org/en/blog/2020/guix-further-reduces-bootstrap-seed-to-25/}};
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), fill=guixorange1, rounded corners=10pt,
               inner sep=10pt, opacity=.8, text opacity=1]
      {\Large{\textbf{60 MiB $\rightarrow$ 0.5 MiB of binary blobs?}}};
    \node [at=(current page.south), anchor=south,
               inner sep=2mm, outer sep=3mm, rounded corners,
               fill=white, opacity=.7, text opacity=1, text=black]
      {\url{https://archive.fosdem.org/2021/schedule/event/gnumes/}};
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \center{\includegraphics[height=.9\paperheight]{images/1f642-smiling-face}};
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 2: The mess we're in
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    % https://raw.githubusercontent.com/pytorch/pytorch/master/docs/source/_static/img/pytorch-logo-dark.svg
    \node<2-> [at=(current page.center), text width=\paperwidth, text
      height=\paperheight, fill=red] {};
    \node [at=(current page.center), shape=star, star points=9, fill=white, inner sep=-35pt]
          {\includegraphics[width=.4\paperwidth]{images/pytorch-logo-dark}};
    \node<3> [at=(current page.center), text width=\paperwidth, text
      height=\paperheight, fill=black, opacity=.6] {};
  \end{tikzpicture}

  \LARGE{
  \begin{itemize}
  \item<3-> on PyPI but \textbf{mostly C++ code}
  \item<3-> \textbf{41 bundled dependencies}
  \item<3-> build system makes \textbf{``unbundling'' hard}
  \item<3-> some builds \textbf{non-deterministic}
  \item<3-> key dependencies have \textbf{no releases, no tags}
  \item<3-> ...
  \end{itemize}
  }
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.west), anchor=north, inner sep=10pt, rotate=90, text=guixdarkgrey]{
      \texttt{guix install python-pytorch}
    };
  \end{tikzpicture}
  \center{\includegraphics[height=\paperheight]{images/pytorch-graph}}
\end{frame}

\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[plain, fragile]
  \begin{semiverbatim}
$ \alert{pip install torch}
\uncover<2->{Collecting torch
  Downloading https://files.pythonhosted.org/packages/69/f2/2c0114a3ba44445de3e6a45c4a2bf33c7f6711774adece8627746380780c/torch-1.9.0-cp38-cp38-manylinux1_x86_64.whl (831.4MB)
     |################################| 831.4MB 91kB/s 
Collecting typing-extensions (from torch)
  Downloading https://files.pythonhosted.org/packages/74/60/18783336cc7fcdd95dae91d73477830aa53f5d3181ae4fe20491d7fc3199/typing_extensions-3.10.0.2-py3-none-any.whl
Installing collected packages: typing-extensions, torch}
  \end{semiverbatim}
\end{frame}

\begin{frame}[plain, fragile]
  \begin{semiverbatim}
$ \alert{wget} -qO /tmp/pytorch.zip \\
   https://files.pythonhosted.org/packages/69/f2/2c\textrm{\dots{}}/torch-1.9.0-cp38-cp38-manylinux1_x86_64.whl
$ \alert{unzip} -l /tmp/pytorch.zip | grep '\\.so'
    29832  06-12-2021 00:37   torch/_dl.cpython-38-x86_64-linux-gnu.so
    29296  06-12-2021 00:37   torch/_C.cpython-38-x86_64-linux-gnu.so
372539384  06-12-2021 00:37   torch/lib/libtorch_cpu.so
    43520  06-12-2021 00:37   torch/lib/\alert<2>{libnvToolsExt}-3965bdd0.so.1
 28964064  06-12-2021 00:37   torch/lib/libtorch_python.so
 46351784  06-12-2021 00:37   torch/lib/libcaffe2_detectron_ops_gpu.so
1159370040  06-12-2021 00:37   torch/lib/libtorch_\alert<2>{cuda}.so
  4862944  06-12-2021 00:37   torch/lib/libnvrtc-builtins.so
   168720  06-12-2021 00:37   torch/lib/\alert<2>{libgomp}-a34b3233.so.1
\textrm{\dots{}}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{fg=white,bg=guixred3}
\begin{frame}[plain]
  \Large{
  \begin{itemize}
  \item includes \textbf{non-free software} without telling you
  \item ``\textbf{random binaries}'': non-verifiable
  \item \textbf{no ``Corresponding Source''} as required by libgomp's LGPLv3
  \item \textbf{developer-uploaded binaries}
  \item \textbf{brittle} (ABI? RUNPATHs? FHS assumptions?)
  \item \dots{}
  \item ... but very convenient
  \end{itemize}
  }

  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.south), anchor=south, text=white] {
      \url{https://hpc.guix.info/blog/2021/09/whats-in-a-package/}
    };

  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{fg=white,bg=white}

\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node<1> [at=(current page.center)]{
      \includegraphics[width=.9\paperwidth]{images/lastpymile}
    };
    \node<1> [at=(current page.south), anchor=south, text=guixdarkgrey]{
      \url{https://doi.org/10.5281/zenodo.4899935}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt, rotate=8,
    drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
    {\includegraphics[width=0.9\paperwidth]{images/npm-left-pad-2016}};
    \node<1> [at=(current page.south), anchor=south, color=guixgrey,
      fill=white, opacity=.5, text opacity=1]
    {\url{https://www.theregister.co.uk/2016/03/23/npm_left_pad_chaos}};

    \node<2-> [at=(current page.center), inner sep=0pt, rotate=-2,
    drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
    {\includegraphics[width=0.9\paperwidth]{images/npm-ua-parser}};
    \node<2> [at=(current page.south), anchor=south, color=guixgrey,
      fill=white, opacity=.5, text opacity=1]
    {\url{https://github.com/faisalman/ua-parser-js/issues/536}};

    \node<3-> [at=(current page.center), inner sep=0pt, rotate=0,
    drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
    {\includegraphics[width=0.9\paperwidth]{images/npm-curl-bash}};
    \node<3> [at=(current page.south), anchor=south, color=guixgrey,
      fill=white, opacity=.5, text opacity=1]
    {\url{https://btao.org/2021/09/09/npm-install-is-curl-bash/}};

    %% \node [at=(current page.center), inner sep=0pt, rotate=0,
    %% drop shadow={opacity=0.5}, draw, color=guixgrey, line width=1pt]
    %% {\includegraphics[width=0.9\paperwidth]{images/
    %% \node<3> [at=(current page.south), anchor=south, color=guixgrey,
    %%   fill=white, opacity=.5, text opacity=1]
    %% {\url{https://btao.org/2021/09/09/npm-install-is-curl-bash/}};

    \node<4> [at=(current page.center)] {
      {\includegraphics[height=.9\paperheight]{images/1f641-worried-face}}
    };

  \end{tikzpicture}
\end{frame}

% TODO: Kubernetes
% TODO: npm
% 
% 

\setbeamercolor{normal text}{fg=white,bg=guixred3}
\begin{frame}[plain, fragile]
  \LARGE{Thesis: \\
    \highlight{packaging practices mirror isolation.}
    \\[10mm]
    (Corollary of Conway's law.)}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Part 3: The way forward
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setbeamercolor{normal text}{fg=blue,bg=guixblue1}
\begin{frame}[plain]
  \Huge{\textbf{From isolated islands \\ to archipelagos?}}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    % https://upload.wikimedia.org/wikipedia/commons/d/d5/%27The_Children_of_Captain_Grant%27_by_%C3%89douard_Riou_004.jpg
    \node [at=(current page.center)]{
      \includegraphics[width=1\paperwidth]{images/message-in-a-bottle}
    };
    \node [at=(current page.center), text width=\paperwidth, text
      height=\paperheight, fill=white, opacity=.3]{};
    \node [at=(current page.south), color=guixred2, anchor=south, inner sep=5mm]{
      \Huge{\textbf{Message to repository maintainers.}}
    };
  \end{tikzpicture}
\end{frame}


\setbeamercolor{normal text}{fg=black,bg=guixdarkgrey}

\begin{frame}
  \LARGE{\textbf{Wish list for repo maintainers:}}

  \begin{enumerate}
    \item (re)move \textbf{non-free software}
    \item \textbf{always provide source}
    \item disallow \textbf{developer-uploaded binaries} % Debian's dirtiest secrets
    \item ensure \textbf{reproducible/verifiable builds}
    \item<2-> \textit{anything else?}  :-)
  \end{enumerate}
\end{frame}

\begin{frame}[plain, fragile]

  \large{
  % Generated by package-breakdown.scm.
  \begin{tabular}{lrr}
    \textbf{Guix packages} (commit bd41e59) & 19,291 & \\
\\
    other & 8,491 & 44\% \\
    Rust (Crates) & 2,608 & 14\% \\
    Python (PyPI) & 2,280 & 12\% \\
    R (CRAN, Bioconductor) & 1,860 & 10\% \\
    Emacs Lisp (ELPA, MELPA) & 1,084 & 6\% \\
    Perl (CPAN) & 778 & 4\% \\
    Haskell (Hackage, Stackage) & 683 & 4\% \\
    Ruby (Gems) & 413 & 2\% \\
    Go & 337 & 2\% \\
    \TeX{} Live & 288 & 1\% \\
    Julia & 248 & 1\% \\
    OCaml + Coq (OPAM) & 221 & 1\% \\
  \end{tabular}

  }
\end{frame}

\begin{frame}[plain, fragile]
  \begin{semiverbatim}
\uncover<1-2>{$ \alert{guix import pypi} webasset}
\uncover<2->{(\alert{package}
  (name "python-webassets")
  (version "2.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "webassets" version))
      (sha256
        (base32 "1kc1042jydgk54xpgcp0r1ib4gys91nhy285jzfcxj3pfqrk4w8n"))))
  (build-system python-build-system)
  (home-page "http://github.com/miracle2k/webassets/")\only<3->{
  (\alert{native-inputs} (list python-jinja2 python-mock
                       python-nose python-pytest))
  (\alert{arguments} \textrm{\dots{}})  ;\textit{actually run tests}}
  (synopsis
    "Media asset management for Python, with glue code for various web frameworks")
  (description
    "Media asset management for Python, with glue code for various web frameworks")
  (license license:bsd-3))}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{fg=blue,bg=guixblue1}
\begin{frame}[plain]
  \Huge{\textbf{How ``good''\\is package repository data?}}
\end{frame}
\setbeamercolor{normal text}{fg=blue,bg=guixdarkgrey}

\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node (url) [at=(current page.south), anchor=south, text=white]{
      \url{https://lists.gnu.org/archive/html/guix-devel/2021-10/msg00297.html}
    };
    \node [at=(url.north), anchor=south, text=white]{
      $^*$ obtained by re-running \texttt{guix import} and comparing the output
    };
  \end{tikzpicture}

  \Large{
    \begin{tabular}{lr}
      \textbf{Repository} & \textbf{Accurate} package data \\
      \\
      CRAN & 85\%$^*$ \\
      Crates & 81\%$^*$ \\
      PyPI & 31\%$^*$ \\
      \textit{ELPA} & \textit{(80\%?)} \\
    \end{tabular}
  }
\end{frame}

\begin{frame}
  \LARGE{\textbf{Common repository issues:}}

  \begin{itemize}
    \item \textbf{hosted source differs} from upstream source
    \item \textbf{missing foreign-language dependencies}
    \item \textbf{missing test dependencies} (PyTest, etc.)
    \item \textbf{unknown test procedure} (\texttt{pytest xyz},
      etc.)
    \item ...
  \end{itemize}
\end{frame}

\begin{frame}
  \LARGE{\textbf{Wish list for repo maintainers:}}

  \begin{enumerate}
    \item (re)move \textbf{non-free software}
    \item \textbf{always provide source}
    \item disallow \textbf{developer-uploaded binaries} % Debian's dirtiest secrets
    \item ensure \textbf{reproducible/verifiable builds}
    \item<2-> \textbf{accurate} package data (dependencies, etc.)
    \item<3-> accurate \textbf{licensing info}
    \item<3-> nice descriptions/synopses :-)
  \end{enumerate}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node<1> [at=(current page.center)]{
      \includegraphics[width=.8\paperwidth]{images/microsoft-supply-chain}
    };
    \node<1> [at=(current page.south), anchor=south, text=guixdarkgrey]{
      \url{https://github.com/microsoft/Secure-Supply-Chain}
    };

    \node<2> [at=(current page.center)]{
      \includegraphics[width=.8\paperwidth]{images/executive-order-1}
    };
    \node<3> [at=(current page.center)]{
      \includegraphics[height=.8\paperheight]{images/executive-order-2}
    };
    \node<2-3> [at=(current page.south), anchor=south,
      text=guixdarkgrey, text width=0.9\paperwidth]{
      \url{https://www.whitehouse.gov/briefing-room/presidential-actions/2021/05/12/executive-order-on-improving-the-nations-cybersecurity/}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{fg=blue,bg=guixblue1}
\begin{frame}[plain]
  \Huge{\textbf{Package managers \\
      are the source \emph{and} solution \\
      to supply chain issues.}}
\end{frame}

\setbeamercolor{normal text}{bg=black}
\begin{frame}[plain]

\vfill{
  \vspace{1.5cm}
  \center{\includegraphics[width=0.3\textwidth]{images/Guix-white}}\\[1.0cm]
  {\alert{\url{https://guix.gnu.org/}}}\hfill{\texttt{ludo@gnu.org}}
}

\end{frame}

\begin{frame}{}
  \begin{textblock}{12}(2, 6)
    \tiny{
      Copyright \copyright{} 2010, 2012--2021 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{https://gnu.org/s/guix/graphics}.
      \\[1.5mm]
      Parcel image from
      \url{https://thumbs.dreamstime.com/z/parcel-illustration-drawing-engraving-ink-line-art-vector-what-made-pencil-paper-then-was-digitalized-143335396.jpg}
      \\[1.5mm]
      % https://commons.wikimedia.org/wiki/File:%27The_Children_of_Captain_Grant%27_by_%C3%89douard_Riou_004.jpg
      Message-in-a-bottle picture by Édouard Riou, public domain
      (Wikimedia Commons).
      \\[1.5mm]
      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{https://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{https://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{https://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "francais"
% compile-command: "guix time-machine --commit=c81457a5883ea43950eb2ecdcbb58a5b144bcd11 -- environment --ad-hoc texlive rubber -- rubber --pdf talk.tex"
% End:

%%  LocalWords:  Reproducibility
