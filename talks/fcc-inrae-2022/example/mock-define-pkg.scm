(package                 ; definition du noeud python
  (name "python")
  (version "3.9.9")
  (source ... )                   ; -> Gitlab, etc.
  (build-system gnu-build-system) ; ./configure; make; install
  (arguments ... )                ; options de production
  (inputs (list ...)))   ; liste d'autres noeuds -> graphe (DAG)
