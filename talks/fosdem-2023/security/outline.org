
* goal: guix pull

* TUF?
* background
** package definition in Git
** workflow
** reproducible builds
** bootstrappable builds
** back to workflow
* how to authenticate a Git checkout?
** "verified" badges, good?
** meaning of "authenticate"
** requirements: changing authorized parties, in-band, off-line
** solution: .guix-authorizations
** authorization invariant
** keyring
** establishing trust: introduction
** putting it all together
** downgrades
* conclusion
** patchwork vs. unified deployment tooling

** verification vs. attestation

  - cf. [[https://docs.sigstore.dev/rekor/overview/][sigstore's rekor]]

** version strings vs. position in commit history
* COMMENT notes
** sigstore

  - [[https://docs.sigstore.dev/cosign/sign/][sign artifiacts with cosign]]
  - [[https://docs.sigstore.dev/gitsign/overview/][gitsign]] (not OpenPGP)
  - [[https://docs.sigstore.dev/rekor/overview/][rekor: immutable, tamper-resistant ledger of metadata generated within a software project’s supply chain.]]

* COMMENT illustrations

** seals

  - https://fr.wikipedia.org/wiki/Sceau#/media/Fichier:Sigillo_in_argento_famiglia_Ciciarelli_de_Cicerello.jpg (dark)
  - https://fr.wikipedia.org/wiki/Cire_%C3%A0_cacheter#/media/Fichier:Cire_%C3%A0_cacheter_du_sceau_de_la_Famille_Ciciarelli.jpg (dark)
  - https://commons.wikimedia.org/wiki/Category:Wax_sealed_postal_covers?uselang=fr#/media/File:1951_Switzerland_-_Luzerner_Landbank_Grosswangen_seals.jpg
  - https://commons.wikimedia.org/wiki/Category:Wax_sealed_postal_covers_of_Russia?uselang=fr#/media/File:Riga_-_Burtnieki_1848-05-01_church_letter_reverse.jpg (white)
  - https://commons.wikimedia.org/wiki/Category:Wax_sealed_postal_covers_of_the_Netherlands?uselang=fr#/media/File:Netherlands_1932-10-1_money_letter_NVPH_193_SF_reverse.jpg (white)
  - https://commons.wikimedia.org/wiki/Category:Rubber_stamps?uselang=fr#/media/File:Wikitrip_to_GI_RAS_(2019-12-19)_98.jpg
    (rubber stamp)
  - https://fr.wikipedia.org/wiki/Sceau#/media/Fichier:Siegel.jpg
  - https://en.wikipedia.org/wiki/Seal_(emblem)#/media/File:Bundesbrief.jpg
  - https://commons.wikimedia.org/wiki/Category:Sealing_wax?uselang=fr#/media/File:Sigillo.svg (synth)
  - https://commons.wikimedia.org/wiki/Category:Sealing_wax?uselang=fr#/media/File:Gewerbeverein_Koenigreich_Hannover_Direktion_1846_Wachssiegel.jpg

  - general: https://commons.wikimedia.org/wiki/Category:Sealing_wax?uselang=fr
