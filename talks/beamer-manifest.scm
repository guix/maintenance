;;; Released under the GNU GPLv3 or any later version.
;;; Copyright © 2021, 2023-2024 Ludovic Courtès <ludo@gnu.org>

;; Manifest to create an environment to build LaTeX/Beamer slides.  Inspired
;; by <https://github.com/etu/presentations/blob/master/shell.nix#L4-L15>.

(specifications->manifest
 '("rubber"

   "texlive-scheme-basic" "texlive-beamer"
   "texlive-ec" "texlive-etoolbox"
   "texlive-ulem"  "texlive-capt-of"
   "texlive-wrapfig" "texlive-geometry"
   "texlive-ms" "texlive-graphics"
   "texlive-pgf" "texlive-translator"
   "texlive-xkeyval" "texlive-mweights" "texlive-fontaxes"
   "texlive-textpos" "texlive-fancyvrb" "texlive-xifthen"
   "texlive-ifmtarg" "texlive-upquote"
   "texlive-babel-french"

   ;; Additional fonts.
   "texlive-cm-super" "texlive-amsfonts"
   "texlive-fira" "texlive-inconsolata"))
