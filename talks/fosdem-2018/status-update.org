#+TITLE: GNU Guix—State of affairs

* Welcome, Guix!
* acknowledgments
 - the organizers: Manolis Ragkousis and Pjotr Prins
 - the sponsors: GeneNetwork and Guix Europe
 - the audience: you!
* great 2017 achievements
** Guix
 - aarch64 port
 - steady stream of security fixes
 - ‘guix pack’
 - packages (+2000), services
 - ~75% reproducible packages
 - build systems: go, font, meson, etc.
 - better error reporting
 - switch to Guile 2.2!
 - installer script
** GuixSD
 - UEFI support
 - containerized system services
 - 'guix system disk-image -t iso9660'
 - multiple bootloader support: GRUB, U-Boot, etc.
 - GuixSD on ARMv7 \o/
** infrastructure
*** berlin.guixsd.org — thanks, MDC & Ricardo!
*** Cuirass is getting (much) better + ‘guix publish’
** events
*** releases
**** only two! (May and December)
*** some blog posts
*** Guix-HPC announced in May
*** talks (see https://www.gnu.org/software/guix/blog/tags/talks/)
 FOSDEM (Feb., BE)
 BOB Konferenz (Feb., BE)
 BOSC (July, CZ)
 RSE (Sep., UK)
 freenode #live (Oct., UK)
 GPCE (Oct., CA)
 Reproducible Build Summit (Oct., DE)
** community
 - ~40 monthly contributors (we reached a plateau?)
 - 1 GSoC student (reepca, on the daemon rewrite in Scheme)
 - guix-patches backlog is okay but needs love :-)
 - Ricardo’s “Mumi” will help?
* challenges for 2018
 - merge ‘core-updates’ more often
   - corollary: make build farm more efficient
     - corollary: improve Cuirass
     - corollary: add a monitoring service on build machines
 - ‘guix pull’ faster (see ‘wip-pull-reload’)
 - grafts…
 - channels
 - recursive ‘guix import’
 - live import: ‘guix package -i cpan!Foo::Bar’
 - help on less-technical things: events, GSoC, web site, blog, etc.
 - panel later today!
