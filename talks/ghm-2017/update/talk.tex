% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}
\usepackage{helvet}
\usepackage{multimedia}         % movie
\usecolortheme{seagull}         % white on black

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for '\Verb'
\usepackage{xifthen}            % for '\isempty'

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{Guix, GuixSD, and getting to 1.0}

\author{Ludovic Courtès}
\date{\small{GNU Hackers Meeting\\Knüllwald-Niederbeisheim, August 2017}}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}

\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}

\maketitle

\begin{frame}{agenda}
  \Huge{
    \begin{itemize}
      \item Seriously, who cares?
      \item What's new?
      \item What's missing?
      \item What's hot?!
    \end{itemize}
  }
\end{frame}

\begin{frame}[fragile]

  \begin{semiverbatim}
\$ guix package -i gcc-toolchain coreutils sed grep
\textrm{...}

\$ eval `guix package --search-paths`
\textrm{...}

\$ guix package --manifest=my-software.scm
\textrm{...}
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node[rounded corners=4, text centered, fill=guixorange1,
          inner sep=4mm, opacity=.75, text opacity=1,] at (current page) {
      % This is the same video as
      % <https://audio-video.gnu.org/video/misc/2016-07__GNU_Guix_Demo_2.webm>.
      \movie[autostart, externalviewer]{\Large{$\blacktriangleright$}}{/data/video/guix/demo.webm}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \vspace{-1.2cm}
    \small{
(\alert{operating-system}
  (host-name "schememachine")
  (timezone "Europe/Berlin")
  (locale "de_DE.utf8")
  (bootloader (grub-configuration (device "/dev/sda")))
  (file-systems (cons (\alert{file-system}
                        (device "my-root")
                        (title 'label)
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))
  (users (cons (\alert{user-account}
                 (name "alice")
                 (group "users")
                 (home-directory "/home/alice"))
               %base-user-accounts))
  (services (cons* (service dhcp-client-service-type)
                   (service openssh-service-type
                            (openssh-configuration
                              (port-number 2222)))
                   %base-services)))
    }
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{So, is it relevant?}}
  \\[2cm]
  \large{\emph{spoiler:} yes it is!}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}
  \LARGE{
    \begin{enumerate}
    \item transactional, per-user package manager
      
    \item{software environment manager
      \begin{itemize}
      \item like virtualenv, ``modules'', Vagrant, Docker, etc.
      \end{itemize}}
    \item{VM/container provisioning tool
      \begin{itemize}
      \item like \texttt{Dockerfile}s, but higher-level
      \end{itemize}}
    \item{OS deployment tool
      \begin{itemize}
      \item (almost) like Ansible, Chef, Puppet, Propellor, etc.
      \item ... and kinda like FreedomBox, Internet Cube/YunoHost
      \end{itemize}}
    \end{enumerate}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{What's new?}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{fg=white,bg=guixgrey}
\begin{frame}[plain]
  \Huge{GuixSD surely works for you now! :-)}
  \\[2cm]
  \large{UEFI, LUKS on root, GNOME, 73 services, etc.}
\end{frame}

\begin{frame}[plain]
  \Huge{~6,000 packages,
    \\
    ~75\% bit-reproducible}
  \\[2cm]
  \large{... and getting better}
\end{frame}

% TODO: add screenshot of reproducibility.html

\setbeamercolor{normal text}{fg=white,bg=black}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
    $ \alert{guix pack} guile emacs geiser
    \textrm{...}
    /gnu/store/\textrm{...}-pack.tar.gz
    }
  \end{semiverbatim}
\end{frame}

\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/guile}
  % Guile 2.2
  % twice as fast, eats less RAM
  % ... but new bugs
\end{frame}

\begin{frame}[plain]{and also...}
  \Large{
    \begin{itemize}
    \item Guix on \highlight{aarch64}
    \item \highlight{containerized} system services
    \item \highlight{multiple bootloader} support
    \item \texttt{guix system disk-image} supports \highlight{ISO~9660}
    \item \highlight{Git integration} for \texttt{guix pull}
    \item MinGW, PowerPC, and Alpha (!) \highlight{cross-compilation targets}
    \item ...
    \end{itemize}
  }
\end{frame}

\begin{frame}
  \LARGE{\texttt{updating the list of substitutes...}}
\end{frame}

\setbeamercolor{normal text}{fg=white,bg=guixgrey}
\begin{frame}[fragile]
  \center{\Huge{\texttt{berlin.guixsd.org}}}

  \begin{tikzpicture}[overlay]
    \node<2>[rounded corners=4, text centered, anchor=north,
          fill=guixorange1, text width=13cm,
          inner sep=5mm, opacity=.75, text opacity=1,
          drop shadow={opacity=0.5}] at (current page.center) {
            \textbf{\LARGE{Thank you, Max Delbrück Center \& Ricardo!}}
          };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{What's missing for 1.0?}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{fg=white,bg=guixgrey}
\begin{frame}[plain]
  \Huge{\#1: faster \texttt{guix pull}, \\
    authenticated commits}
\end{frame}

\begin{frame}[plain]
  \Huge{\#2: stable, self-hosted infrastructure: Cuirass, etc.}
\end{frame}

\begin{frame}[plain]
  \Huge{\#3: performance \& UI improvements}
\end{frame}

\begin{frame}[plain]
  \Huge{\#4: fewer glitches in our workflow...}

  \Large{
  \begin{itemize}
  \item merge \texttt{core-updates} more quickly...
  \item \highlight{big set of updates} frowned upon (GHC, Perl, Python, ...)
  \item tame the \highlight{patch \& bug queues}
  \item \highlight{security updates} need more people
  \item \highlight{build farm admin} needs more automation
  \item ...
  \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}
  \Huge{\textbf{What's hot?!}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{fg=white,bg=guixgrey}
\begin{frame}
  \Huge{high-performance computing (HPC)}
  \Large{
    \begin{itemize}
    \item<2-> better Guix setups on clusters
    \item<2-> better performance
    \item<2-> \highlight{reproducible} environments
    \item<2-> \highlight{Guix Workflow Language} (Roel Janssen)
    \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{fg=white,bg=guixgrey}
\begin{frame}
  \Huge{distributed Guix \& ``GuixOps''}
  \\[1.5cm]
  \Large{
    \begin{itemize}
    \item<2-> \texttt{GUIX\_DAEMON\_SOCKET=ssh://host.example.org}
    \item<2-> + \highlight{remote code evaluation} through Guile-SSH
    \item<2-> = \texttt{guix system reconfigure --remote=host} ?
    \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{fg=white,bg=black}
\begin{frame}
  \Large{
  \begin{itemize}
    \item \highlight{the potluck} (Alex's talk tomorrow!)
    \item \highlight{GuixSD on ARM?} (Mathieu's talk today!)
    \item \highlight{\texttt{bootstrappable.org}} \& Mes (Ricardo's talk
      tomorrow!)
    \item \highlight{GuixSD ncurses installer} (John's FOSDEM talk)
    \item \highlight{build daemon written in Guile} (reepca, GSoC 2017)
    \item get \highlight{substitutes from your neighbors} with Avahi
    \item search \& \highlight{on-line doc of system services}
    \item \textit{your project here}
  \end{itemize}
  }
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[plain]
  \Huge{\textbf{Join us now!}
  \\Get yourself a sticker too!}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[plain]

\vfill{
  \vspace{2.5cm}
  \center{\includegraphics[width=0.2\textwidth]{images/GuixSD}}\\[1.0cm]
  \texttt{ludo@gnu.org}\hfill{\alert{\url{https://gnu.org/software/guix/}}}
  \\
}
\end{frame}


\begin{frame}{}

  \begin{textblock}{12}(2, 8)
    \tiny{
      Copyright \copyright{} 2010, 2012--2017 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{http://gnu.org/s/guix/graphics}

      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{http://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{http://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{http://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "american"
% compile-command: "rubber --pdf talk.tex"
% End:

%%  LocalWords:  Reproducibility
