% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

% Make sure URLs are broken on hyphens.
% See <https://tex.stackexchange.com/questions/3033/forcing-linebreaks-in-url>.
\RequirePackage[hyphens]{url}

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}

% Nice sans-serif font.
\usepackage[sfdefault,lining]{FiraSans} %% option 'sfdefault' activates Fira Sans as the default text font
\renewcommand*\oldstylenums[1]{{\firaoldstyle #1}}

% Nice monospace font.
\usepackage{inconsolata}
%% \renewcommand*\familydefault{\ttdefault} %% Only if the base font of the document is to be typewriter style
\usepackage[T1]{fontenc}

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}

\usecolortheme{seagull}         % white on black
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning,backgrounds}
\usepackage{fancyvrb}           % for '\Verb'
\usepackage{xifthen}            % for '\isempty'

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\definecolor{rescienceyellow}{RGB}{254,246,91}

\definecolor{sifblue}{RGB}{83,171,221}
\definecolor{sifgreen}{RGB}{34,204,127}

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{Guix: Reproducible Software Deployment for Reproducible Research}

\author{Ludovic Courtès}
\date{30 November 2021}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}



% https://twitter.com/fermatslibrary/status/1034065248989466624
% "Feynman's notebook"
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center)] {
      \includegraphics[width=1.2\textwidth]{images/feynman-notebook}
    };
    \node [at=(current page.center), fill=guixorange2, opacity=.4,
      text width=1.3\textwidth, text height=\textheight] {
    };
    \node [at=(current page.south east), anchor=south east, inner sep=5mm] {
      {\includegraphics[width=0.2\paperwidth]{images/inria-white-2019}}
    };
  \end{tikzpicture}

  \vspace{12mm}
  \Huge{\textbf{Guix: Reproducible \\
      Software Deployment \\
      for Reproducible Research}
  \\[15mm]
  \large{Ludovic Courtès}
  \\[2mm]
  \alert{Software Heritage Fifth Anniversary}
  \\[1.5mm]
  \oldstylenums{30 November 2021}}
  \vfill{}

\end{frame}



\setbeamercolor{normal text}{fg=black,bg=white}
% http://www.nature.com/ngeo/journal/v7/n11/full/ngeo2294.html
%% \screenshot{images/nature-transparency}

% https://www.nature.com/nmeth/journal/v12/n12/full/nmeth.3686.html
%% \screenshot{images/nature-reviewing-computational-methods}
% http://blogs.nature.com/methagora/2014/02/guidelines-for-algorithms-and-software-in-nature-methods.html

% http://www.acm.org/publications/policies/artifact-review-badging
%% \screenshot[height=\paperheight]{images/acm-artifact-review-and-badging}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]{
      \includegraphics[height=.8\textheight]{images/acm-artifacts-functional}
    };
    \node [at=(current page.south), anchor=south,
      text=guixdarkgrey, fill=white, text opacity=1]{
      \small{\url{https://www.acm.org/publications/policies/artifact-review-badging}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-1}
\end{frame}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-2}
\end{frame}
\begin{frame}[plain]
  \includegraphics[width=\textwidth]{images/big-picture-3}
\end{frame}


\setbeamercolor{normal text}{bg=white}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[width=1.3\textwidth]{images/smoothie}};
    \node [at=(current page.south east), anchor=south east, text=guixgrey]
      {\small{courtesy of Ricardo Wurmus}};
  \end{tikzpicture}
\end{frame}

% https://xkcd.com/1988/

\setbeamercolor{normal text}{bg=white,fg=guixorange1}
\begin{frame}[fragile]
  \begin{tikzpicture}[overlay]
    \node(logo) [at=(current page.center), inner sep=0pt]
      {\includegraphics[width=.8\textwidth]{images/guixhpc-logo-transparent-white}};
    %% \node [at=(logo.south), anchor=north, text=black, inner sep=10pt]
    %%   {\Large{\textbf{Reproducible software deployment\\for high-performance computing.}}};
    \node [at=(current page.south), anchor=south, text=guixdarkgrey, inner sep=20pt]
      {\url{https://hpc.guix.info}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setbeamercolor{normal text}{bg=guixtaupe}

\begin{frame}[fragile]

  \begin{semiverbatim}
    \LARGE{
guix \alert{install} python python-keras

guix package \alert{--roll-back}

guix \alert{environment} --ad-hoc \\
     python python-scipy python-scikit-learn
}
  \end{semiverbatim}
\end{frame}


\setbeamercolor{normal text}{bg=guixtaupe}
\begin{frame}[fragile]
  \begin{semiverbatim}
    \LARGE{
guix package \alert{--manifest}=my-packages.scm



    (\alert{specifications->manifest}
      '("python" "python-scikit-learn"
        "python-matplotlib"))
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
bob@laptop$ guix package \alert{--manifest}=my-packages.scm
bob@laptop$ guix \alert{describe}
  guix cabba9e
    repository URL: https://git.sv.gnu.org/git/guix.git
    commit: cabba9e15900d20927c1f69c6c87d7d2a62040fe

\pause


alice@supercomp$ guix \alert{pull} --commit=cabba9e
alice@supercomp$ guix package \alert{--manifest}=my-packages.scm
}
  \end{semiverbatim}
\end{frame}


\begin{frame}[fragile]
  \begin{tikzpicture}[remember picture, overlay]
    % https://commons.wikimedia.org/wiki/File:TeamTimeCar.com-BTTF_DeLorean_Time_Machine-OtoGodfrey.com-JMortonPhoto.com-07.jpg
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=\paperwidth]{images/delorean}};
    \node [rounded corners=4, text centered, anchor=north,
           text width=10cm,
          inner sep=3mm, opacity=.75, text opacity=1]
      at (current page.center) {
            \textbf{\Huge{travel in space \emph{and} time!}}
          };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
    \LARGE{
guix \alert{time-machine} --commit=cabba9e -- \\
     install python
    }
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
(define python-scikit-learn
  (\alert{package}
    (name "python-scikit-learn")
    (home-page ""https://github.com/scikit-learn/scikit-learn")
    (\alert{source} (origin
              (method git-fetch)
              (uri (git-reference
                     (\alert{url} home-page)
                     (\alert{commit} "2f30ff07a")\tikz{\node(commit){};}
                     (recursive? #t)))
              (sha256
               (base32
                "106rf402cvfdhc2yf\textrm{...}"))))
    \textrm{...}))
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node<2->(swh) [inner sep=3mm, rounded corners, fill=black,
                    opacity=.3, text opacity=1] at (12,5) {
       % https://annex.softwareheritage.org/public/logo/
       \includegraphics[width=0.33\textwidth]{images/software-heritage-logo-title-white}
    };
    \node<2->      [at=(current page.south), anchor=south,
                    inner sep=2mm, rounded corners, fill=black, text width=13cm,
                    opacity=.3, text opacity=1] {
       \url{https://www.softwareheritage.org/2019/04/18/software-heritage-and-gnu-guix-join-forces-to-enable-long-term-reproducibility/}
    };

    \path<2->[very thick, draw=guixorange1]
      (swh) edge [out=-90, in=0, ->] (commit);
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain, fragile]
  \begin{semiverbatim}
  \Large{
$ guix lint -c \alert{archival} python-scikit-learn
scheduled Software Heritage archival
  }
  \end{semiverbatim}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node(sources) [at=(current page.north west), anchor=north west, inner
      sep=5mm, rounded corners, outer sep=5mm, fill=white, opacity=.3, text opacity=1] {
      \url{https://guix.gnu.org/sources.json}
    };
    \node(swh) [at=(current page.south east), anchor=south east, inner
      sep=5mm, outer sep=5mm, rounded corners, fill=white, opacity=.3, text opacity=1] {
       % https://annex.softwareheritage.org/public/logo/
       \includegraphics[width=0.33\textwidth]{images/software-heritage-logo-title-white}
    };
    \node<2-> [at=(current page.south west), anchor=south west, inner
      sep=5mm] { \Large{\textbf{Thanks, Tweag \& SWH!}} };
    
    \path[very thick, draw=guixorange1]
      (sources) edge [out=-90, in=90, ->] (swh.north);
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}[plain, fragile]
  \begin{semiverbatim}
(package
  (name "openblas")
  (version "0.3.9")
  (source
   (origin
     (method url-fetch)
     (uri (string-append "https://sourceforge.net/openblas/"
                         "/OpenBLAS%20" version "\tikz[baseline]{\node(targz)[anchor=base]{.\alert{tar.gz}};}"))
     (\alert{sha256}\tikz[baseline]{\node(hash)[anchor=base]{};}
       (base32
        "14iz9xnrb9x\textrm{...}"))))
  \textrm{...})))
  \end{semiverbatim}

  \begin{tikzpicture}[overlay]
    \node(label) at (9,6) [text width=55mm, rounded corners, fill=white,
      opacity=.3, text opacity=1, inner sep=3mm] {
      66\% of package source code distributed as ``tarballs''
    };
    \path[very thick, draw=guixorange1]
      (label) edge [out=-90, in=90, ->] (targz);

    \node<2->(question) at (9,1) [text width=80mm, rounded corners,
      fill=white, opacity=.3, text opacity=1, inner sep=3mm] {
      \Large{\textbf{How do we reconstruct tarballs\\[1.5mm] from archived
          content?}}
    };
    \path<2->[very thick, draw=guixorange1]
      (question) edge [out=90, in=0, ->] (hash.east);
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixblue1}
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[
        crucial/.style = {
          text width=35mm, minimum height=4cm,
          text centered, rounded corners,
          fill=white, text=black,
          draw=guixorange1, line width=2mm
        },
        input/.style = {
          shape=circle,
          fill=white, text=black,
          draw=guixorange1, very thick
        },
        important/.style = {
          text width=65mm, minimum height=2cm,
          text centered, rounded corners,
          fill=white, text=black
        }]
    \matrix[row sep=3mm, column sep=12mm] {
      & & \node<2->(swh){\includegraphics[width=0.4\textwidth]{images/software-heritage-logo-title-white}};
      \\
      \node(tarball) [input] {tar.gz}; & \node(disarchive) [crucial] {\Large{\textbf{Disarchive}} \\[2mm] \texttt{\only<1-2>{disassemble}\only<3>{assemble}}}; &  \\
      \\
      & & \node<2->(database) [important] {\textbf{tarball metadata} \\[2mm]  \url{https://disarchive.guix.gnu.org}};
      \\
    };

    \begin{scope}[on background layer]
    \uncover<3->{\node[shape=star, star points=9, fill=guixorange2,
        inner sep=5mm, opacity=.8, at=(tarball.center)] {};}
    \end{scope}

    \path<1-2>[very thick, draw=guixorange1] (tarball) edge [->] (disarchive);
    \path<2>[very thick, draw=guixorange1] (disarchive) edge [->, out=0, in=-120] (swh.south);
    \path<2>[very thick, draw=guixorange1] (disarchive) edge [->, out=0] (database.north);

    \path<3>[very thick, draw=guixorange1] (tarball) edge [<-] (disarchive);
    \path<3>[very thick, draw=guixorange1] (disarchive) edge [<-, out=0, in=-120] (swh.south);
    \path<3>[very thick, draw=guixorange1] (disarchive) edge [<-, out=0] (database.north);
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=white}

\begin{frame}[plain]
  \begin{tikzpicture}[overlay]
    \node [at=(current page.center)] {
      \includegraphics[width=0.8\textwidth]{images/pog-report-20211022}
    };

    \node [at=(current page.west), anchor=north, text=guixdarkgrey,
      rotate=90, inner sep=5mm] {
      \Large{\textbf{Report on the Preservation of Guix}}
    };
    \node(plot) [at=(current page.south), anchor=south, text=guixdarkgrey]{
      \url{https://ngyro.com/pog-reports/2021-10-22}
    };

    \node<2> [fill=white, opacity=.5, text opacity=1, text=black,
      rounded corners, outer sep=3mm, inner sep=5mm] at (10,- 2) {
      \large{\textbf{73\% archived}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixtaupe}
\begin{frame}[plain]
  \huge{\textbf{On-going work}}
  \\[2cm]
  \Large{
  \begin{itemize}
    \item increasing \textbf{tarball coverage} in Disarchive
    \item \textbf{replicating} the Disarchive database
    \item archiving source from \textbf{past Guix revisions}
    \item ...
    \item getting to \textbf{100\% Software Heritage coverage}
  \end{itemize}
  }
\end{frame}

\begin{frame}[plain]
  \huge{\textbf{Special thanks}}
  \\[2cm]

  \LARGE{
  \begin{itemize}
  \item Timothy Sample
  \item Simon Tournier
  \item Antoine Eiche
  \item ... and the Software Heritage team!
  \end{itemize}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=\textwidth]{images/big-picture-3}
\screenshot[height=\textheight]{images/end-to-end-2}

\setbeamercolor{normal text}{bg=guixblue1}
\begin{frame}[fragile]
  \vspace{10mm}
  \Huge{Deployment tools should help \\[2mm]
    research \emph{improve} \\[2mm]
    \textbf{provenance tracking}, \\[2mm]
    \textbf{reproducibility}, \\[2mm] and \textbf{experimentation}.}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamercolor{normal text}{bg=black}
\begin{frame}[plain]

\vfill{
  \vspace{3cm}
  \center{\includegraphics[width=0.5\textwidth]{images/guixhpc-logo-transparent}}\\[1.0cm]
  \texttt{ludovic.courtes@inria.fr |} @GuixHPC
  \center{\alert{\url{https://hpc.guix.info}}}
  \\[1cm]
}
\end{frame}

\setbeamercolor{normal text}{bg=guixred2}
\begin{frame}
  \Huge{\textbf{Bonus slides!}}
\end{frame}

\setbeamercolor{normal text}{bg=white}

\screenshot{images/guix-scope}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]{
      \includegraphics[width=.9\textwidth]{images/repeatability-study}
    };
    \node [at=(current page.south east), anchor=south east,
      text=guixdarkgrey, fill=white, text opacity=1]{
      \small{\url{http://reproducibility.cs.arizona.edu/}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
\LARGE{
guix pack hwloc \\
  \alert{--with-source}=./hwloc-2.1rc1.tar.gz


guix install mumps \\
  \alert{--with-input}=scotch=pt-scotch
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=0.95\paperwidth]{images/snap-crypto-miner}};
    \node [at=(current page.south east), anchor=south east,
           text=black, text opacity=1, fill=white]{
      \small{\url{https://github.com/canonical-websites/snapcraft.io/issues/651}}
    };
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=0.9\paperwidth]{images/lwn-docker-hello-world}};
    \node [at=(current page.south east), anchor=south east,
           text=white, fill=black, text opacity=1]{
      \small{\url{https://lwn.net/Articles/752982/}}
    };
  \end{tikzpicture}
\end{frame}

\setbeamercolor{normal text}{bg=black}
\begin{frame}{}
  \begin{textblock}{12}(2, 5)
    \tiny{
      Copyright \copyright{} 2010, 2012--2021 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{https://gnu.org/s/guix/graphics}.
      \\[1.5mm]
      Feynman's notebook picture from \url{https://fermatslibrary.com}
      \\[1.5mm]
      Smoothie image and hexagon image \copyright{} 2019 Ricardo Wurmus,
      CC-BY-SA 4.0.
      \\[1.5mm]
      Hand-drawn arrows by Freepik from flaticon.com.
      \\[1.5mm]
      DeLorean time machine picture \copyright{} 2014 Oto Godfrey and
      Justin Morton, CC-BY-SA 4.0,
      \url{https://commons.wikimedia.org/wiki/File:TeamTimeCar.com-BTTF_DeLorean_Time_Machine-OtoGodfrey.com-JMortonPhoto.com-07.jpg}.
      \\[1.5mm]
      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{https://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{https://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{https://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "francais"
% compile-command: "guix shell -m ../beamer-manifest.scm -- rubber --pdf talk.tex"
% End:

%%  LocalWords:  Reproducibility
