% The comment below tells Rubber to compile the .dot files.
%
% rubber: module graphics
% rubber: rules rules.ini

\documentclass[aspectratio=169]{beamer}

\usetheme{default}

\usefonttheme{structurebold}
\usepackage{helvet}
\usepackage{multimedia}         % movie
\usecolortheme{seagull}         % white on black

\usepackage[utf8]{inputenc}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref,xspace,multicol}
\usepackage[absolute,overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{arrows,shapes,trees,shadows,positioning}
\usepackage{fancyvrb}           % for \Verb

% Remember the position of every picture.
\tikzstyle{every picture}+=[remember picture]

\tikzset{onslide/.code args={<#1>#2}{%
  \only<#1>{\pgfkeysalso{#2}} % \pgfkeysalso doesn't change the path
}}

% Colors.
\definecolor{guixred1}{RGB}{226,0,38}  % red P
\definecolor{guixorange1}{RGB}{243,154,38}  % guixorange P
\definecolor{guixyellow}{RGB}{254,205,27}  % guixyellow P
\definecolor{guixred2}{RGB}{230,68,57}  % red S
\definecolor{guixred3}{RGB}{115,34,27}  % dark red
\definecolor{guixorange2}{RGB}{236,117,40}  % guixorange S
\definecolor{guixtaupe}{RGB}{134,113,127} % guixtaupe S
\definecolor{guixgrey}{RGB}{91,94,111} % guixgrey S
\definecolor{guixdarkgrey}{RGB}{46,47,55} % guixdarkgrey S
\definecolor{guixblue1}{RGB}{38,109,131} % guixblue S
\definecolor{guixblue2}{RGB}{10,50,80} % guixblue S
\definecolor{guixgreen1}{RGB}{133,146,66} % guixgreen S
\definecolor{guixgreen2}{RGB}{157,193,7} % guixgreen S

\setbeamerfont{title}{size=\huge}
\setbeamerfont{frametitle}{size=\huge}
\setbeamerfont{normal text}{size=\Large}

% White-on-black color theme.
\setbeamercolor{structure}{fg=guixorange1,bg=black}
\setbeamercolor{title}{fg=white,bg=black}
\setbeamercolor{date}{fg=guixorange1,bg=black}
\setbeamercolor{frametitle}{fg=white,bg=black}
\setbeamercolor{titlelike}{fg=white,bg=black}
\setbeamercolor{normal text}{fg=white,bg=black}
\setbeamercolor{alerted text}{fg=guixyellow,bg=black}
\setbeamercolor{section in toc}{fg=white,bg=black}
\setbeamercolor{section in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{subsubsection in toc}{fg=guixorange1,bg=black}
\setbeamercolor{subsubsection in toc shaded}{fg=white,bg=black}
\setbeamercolor{frametitle in toc}{fg=white,bg=black}
\setbeamercolor{local structure}{fg=guixorange1,bg=black}

\newcommand{\highlight}[1]{\alert{\textbf{#1}}}

\title{GNU Guix's Take on a New Approach to Software Distribution}

\author{Ludovic Courtès}
\date{FOSDEM\\ 3 February 2019}

\setbeamertemplate{navigation symbols}{} % remove the navigation bar

\AtBeginSection[]{
  \begin{frame}
    \frametitle{}
    \tableofcontents[currentsection]
  \end{frame} 
}


\newcommand{\screenshot}[2][width=\paperwidth]{
  \begin{frame}[plain]
    \begin{tikzpicture}[remember picture, overlay]
      \node [at=(current page.center), inner sep=0pt]
        {\includegraphics[{#1}]{#2}};
    \end{tikzpicture}
  \end{frame}
}


\begin{document}

\maketitle

% TODO sun + Debian & co.
% https://commons.wikimedia.org/wiki/Category:Sun?uselang=fr#/media/File:%22Sun%22.JPG
% https://en.wikipedia.org/wiki/Zenith#/media/File:Tropical-area-mactan-philippines.jpg
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=\paperwidth]{images/sun}};
    \node [at=(current page.center), text=black,
           text opacity=1, rounded corners=2pt]
          {\LARGE{\textbf{ Slackware {\tt |} Debian {\tt |} Red Hat }}};
  \end{tikzpicture}
\end{frame}


% TODO clouds + VirtualEnv/Spack/EasyBuild/modules
% https://commons.wikimedia.org/wiki/Category:Clouds_from_below?uselang=fr#/media/File:Cloud_(5018750171).jpg
% https://commons.wikimedia.org/wiki/Category:Clouds_from_below?uselang=fr#/media/File:Chigwell_Meadow_Essex_England_-_cumulus_clouds.jpg
% https://commons.wikimedia.org/wiki/Category:Clouds_from_below?uselang=fr#/media/File:Clouds_above_Lordship_Recreation_Ground_Haringey_London_England_1.jpg
% https://commons.wikimedia.org/wiki/Category:Clouds_and_blue_sky?uselang=fr#/media/File:2018_05_Havelland_IMG_1931.JPG
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=1.4\paperwidth]{images/clouds}};
    \node [at=(current page.center), text=white,
           text opacity=1, rounded corners=2pt]
          {\LARGE{\textbf{ modules {\tt |} Spack {\tt |} EasyBuild {\tt
                  |} VirtualEnv }}};
  \end{tikzpicture}
\end{frame}


% TODO dark clouds + pip/CONDA/...
% https://commons.wikimedia.org/wiki/Category:Cumulus_congestus_clouds?uselang=fr#/media/File:Cumulunimbus_IMG_5537.JPG
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=1.4\paperwidth]{images/cumulunimbus}};
    \node [at=(current page.center), text=white,
           text opacity=1, rounded corners=2pt]
          {\LARGE{\textbf{ Ansible {\tt |} Puppet {\tt |} Propellor }}};
  \end{tikzpicture}
\end{frame}

% TODO dark clouds + Ansible/Puppet/...
% https://commons.wikimedia.org/wiki/Category:Stratus_clouds?uselang=fr#/media/File:2018-05-18_18_27_24_Low_stratiform_clouds_(base_near_3,000_feet_AGL)_with_wavy,_bumpy_base_viewed_from_Mercer_County_Route_622_(North_Olden_Avenue)_in_Ewing_Township,_Mercer_County,_New_Jersey.jpg
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=1.4\paperwidth]{images/low-clouds}};
    \node [at=(current page.center), text=white, inner sep=5cm,
           text opacity=1, rounded corners=2pt, fill=black, opacity=.5]
          {\LARGE{\textbf{ pip {\tt |} Cabal {\tt |} Cargo {\tt |} CONDA
          {\tt |} Gradle }}};
  \end{tikzpicture}
\end{frame}


% TODO thunder + Flatpak/snap/Docker/Vagrant
% https://commons.wikimedia.org/wiki/Category:Cloud-to-cloud_lightning#/media/File:004_2018_05_14_Extremes_Wetter.jpg
\begin{frame}[plain, fragile]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=1.4\paperwidth]{images/thunder}};
    \node [at=(current page.center), text=white,
           text opacity=1, rounded corners=2pt]
          {\LARGE{\textbf{ Flatpak {\tt |} snap {\tt |} Docker {\tt |} Vagrant }}};
  \end{tikzpicture}
\end{frame}

% TODO 2048 vuln + Docker license opacity

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}[plain, fragile]
  \center{\Huge{\textbf{Are distros doomed?}}}
  \\[2cm]
  \uncover<2->{\center{Yes!} \par}
  \uncover<3->{\center{No!} \par}
\end{frame}
\setbeamercolor{normal text}{bg=black,fg=white}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
          {\includegraphics[width=0.7\paperwidth]{images/Guix-horizontal-print}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}


% demo
\begin{frame}[fragile]

  \begin{semiverbatim}
    \LARGE{
guix package \alert{-i} gcc-toolchain openmpi hwloc

eval `guix package \alert{--search-paths}=prefix`

guix package \alert{--roll-back}

guix package \alert{--profile}=./experiment \\
     -i gcc-toolchain@5.5 hwloc@1
}
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
    \LARGE{
guix package \alert{--manifest}=my-packages.scm



    (\alert{specifications->manifest}
      '("gcc-toolchain" "emacs"
        "guile" "emacs-geiser"))
}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=guixdarkgrey}
\begin{frame}[fragile]
  \begin{semiverbatim}
    \Large{
bob@laptop$ guix package \alert{--manifest}=my-packages.scm
bob@laptop$ guix \alert{describe}
  guix cabba9e
    repository URL: https://git.sv.gnu.org/git/guix.git
    commit: cabba9e15900d20927c1f69c6c87d7d2a62040fe

\pause


alice@supercomp$ guix \alert{pull} --commit=cabba9e
alice@supercomp$ guix package \alert{--manifest}=my-packages.scm
}
  \end{semiverbatim}

  %% \begin{tikzpicture}[overlay]
  %%   \node<3>[rounded corners=4, text centered, anchor=north,
  %%         fill=guixorange1, text width=7cm,
  %%         inner sep=3mm, opacity=.75, text opacity=1]
  %%     at (current page.center) {
  %%           \textbf{\Large{bit-reproducible \& portable!}}
  %%         };
  %% \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]%{``Virtual environments''}
  \LARGE{
    \begin{semiverbatim}
guix \alert{environment} \dots{}
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=guixred3}
\begin{frame}[fragile]%{Container provisioning}
  \LARGE{
    \begin{semiverbatim}
\$ guix \alert{pack}\only<2>{ --relocatable}\only<3->{ --format=docker} \\
      python python-numpy
\textrm{...}
/gnu/store/\textrm{...}-\only<1-2>{pack.tar.gz}\only<3->{docker-image.tar.gz}
    \end{semiverbatim}
  }
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=.9\paperwidth]{images/docker-guix-lol}
\setbeamercolor{normal text}{fg=white,bg=black}


\begin{frame}[fragile]
  \begin{semiverbatim}
    \vspace{-1cm}
    \small{
(\alert{operating-system}
  (host-name "schememachine")
  (timezone "Europe/Brussels")
  (locale "fr_BE.utf8")
  (bootloader (\alert{bootloader-configuration}
                (bootloader grub-efi-bootloader)
                (target "/boot/efi")))
  (file-systems (cons (\alert{file-system}
                        (device (file-system-label "my-root"))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))
  (users (cons (\alert{user-account}
                 (name "charlie")
                 (group "users")
                 (home-directory "/home/charlie"))
               %base-user-accounts))
  (services (cons* (\alert{service} dhcp-client-service-type)
                   (\alert{service} openssh-service-type)
                   %base-services)))
    }
  \end{semiverbatim}
\end{frame}

\begin{frame}[fragile]
  \begin{semiverbatim}
\$ guix system build config.scm
\textrm{...}   

\$ guix system vm config.scm
\textrm{...}

\$ guix system container config.scm
\textrm{...}

\$ guix system reconfigure config.scm
\textrm{...}
  \end{semiverbatim}
\end{frame}

\setbeamercolor{normal text}{bg=white}
\screenshot[width=.8\paperwidth]{images/reproducible-builds}
\setbeamercolor{normal text}{bg=black}

\setbeamercolor{normal text}{bg=guixblue2}
\begin{frame}[fragile]
  \begin{semiverbatim}
$ \alert{guix challenge} --substitute-urls="https://ci.guix.info https://example.org"
\alert{/gnu/store/\dots{}-openssl-1.0.2d contents differ}:
  local hash: 0725l22\dots{}
  http://ci.guix.info/\dots{}-openssl-1.0.2d: 0725l22\dots{}
  http://example.org/\dots{}-openssl-1.0.2d: 1zy4fma\dots{}
\alert{/gnu/store/\dots{}-git-2.5.0 contents differ}:
  local hash: 00p3bmr\dots{}
  http://ci.guix.info/\dots{}-git-2.5.0: 069nb85\dots{}
  http://example.org/\dots{}-git-2.5.0: 0mdqa9w\dots{}
\alert{/gnu/store/\dots{}-pius-2.1.1 contents differ}:
  local hash: 0k4v3m9\dots{}
  http://ci.guix.info/\dots{}-pius-2.1.1: 0k4v3m9\dots{}
  http://example.org/\dots{}-pius-2.1.1: 1cy25x1\dots{}
  \end{semiverbatim}
\end{frame}
\setbeamercolor{normal text}{bg=black}

% demo guix build foo --check | guix challenge
% Bootstrappable logo

\setbeamercolor{normal text}{bg=white}
\screenshot[width=.8\paperwidth]{images/bootstrappable}
\setbeamercolor{normal text}{bg=black}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[height=\paperheight]{images/bootstrap-graph}};
    \node<2-> [at=(current page.center), anchor=north, inner sep=20pt, text=guixgrey]
      {\Large{\textbf{250 MiB of binary blobs}}};
  \end{tikzpicture}
\end{frame}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt]
      {\includegraphics[height=\paperheight]{images/bootstrap-graph-reduced}};
    \node<1> [at=(current page.center), text=guixgrey]
      {\Large{\textbf{250 MiB $\rightarrow$ 130 MiB of binary blobs}}};
    \node<2-> [at=(current page.center), fill=guixorange1, rounded corners=10pt,
               inner sep=10pt, opacity=.8, text opacity=1]
      {\Large{\textbf{Thank you, Jan Nieuwenhuizen!}}};
  \end{tikzpicture}
\end{frame}

\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), inner sep=0pt, rotate=30]
      {\includegraphics[height=1.1\paperheight]{images/rust-bootstrap}};
    \node<2-> [at=(current page.center), fill=guixorange1, rounded corners=10pt,
               inner sep=10pt, opacity=.8, text opacity=1]
      {\Large{\textbf{Thumbs up, Danny Milosavljevic!}}};
  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{bg=white}
\begin{frame}[plain]
  \begin{tikzpicture}[remember picture, overlay]
    \node [at=(current page.center), fill=guixgrey,
      shape=circle, inner sep=2.2cm, opacity=.8, text opacity=1] {};
    \node [at=(current page.center), fill=guixorange1, rounded corners=10pt,
      shape=circle, inner sep=2cm, opacity=1, text opacity=1] {};
    \node<2-> [at=(current page.center), fill=guixorange1, rounded corners=10pt,
      shape=circle, inner sep=10pt, opacity=0, text opacity=1]
      {\Huge{\textbf{1.0!\uncover<3->{*}}}};
      \node<3-> [at=(current page.south east), anchor=south east,
        fill=white, text=guixgrey, shape=circle, inner sep=10pt, opacity=0, text opacity=1]
      {\large{* almost}};

  \end{tikzpicture}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

% open questions

\setbeamercolor{normal text}{bg=guixblue1}
\begin{frame}
  \Huge{\textbf{Wrap-up.}}
\end{frame}
\setbeamercolor{normal text}{fg=white,bg=black}

\setbeamercolor{normal text}{fg=white,bg=black}
\begin{frame}
  \LARGE{
    \begin{enumerate}
    \item transactional \highlight{package manager}
    \item software \highlight{environment manager}
    \item \highlight{container provisioning} tools
    \item whole system \highlight{configuration management}
    \item<2-> \textbf{Guix cares about your freedom \& security!}
    \end{enumerate}
  }
\end{frame}

% 1.0, features, contributors, etc.
% take-away message


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[plain]

\vfill{
  \vspace{1.5cm}
  \center{\includegraphics[width=0.3\textwidth]{images/Guix-white}}\\[1.0cm]
  \texttt{ludo@gnu.org}\hfill{\alert{\url{https://gnu.org/software/guix/}}}
}

\end{frame}

\begin{frame}{}

  \begin{textblock}{12}(2, 4)
    \tiny{
      Copyright \copyright{} 2010, 2012--2019 Ludovic Courtès \texttt{ludo@gnu.org}.\\[3.0mm]
      GNU Guix logo, CC-BY-SA 4.0, \url{https://gnu.org/s/guix/graphics} \\
      Reproducible Builds logo under CC-BY 3.0,
      \url{https://uracreative.github.io/reproducible-builds-styleguide/visuals/}. \\
      Bootstrappable Builds logo by Ricardo Wurmus,
      \url{https://bootstrappable.org}. \\
      Docker whale image by Ricardo Wurmus.
      \\[1.5mm]
      Picture of the sun under CC-BY-SA 3.0,
      \url{https://commons.wikimedia.org/wiki/File:\%22Sun\%22.JPG}. \\
      Cloud picture 1 under CC-BY-SA 2.0,
      \url{https://commons.wikimedia.org/wiki/File:Cloud_(5018750171).jpg}. \\
      Cloud picture 2 under CC-BY-SA 3.0,
      \url{https://commons.wikimedia.org/wiki/File:Cumulunimbus_IMG_5537.JPG}. \\
      Cloud picture 3 under CC-BY-SA 4.0,
      \url{https://commons.wikimedia.org/wiki/File:2018-05-18_18_27_24_Low_stratiform_clouds_(base_near_3,000_feet_AGL)_with_wavy,_bumpy_base_viewed_from_Mercer_County_Route_622_(North_Olden_Avenue)_in_Ewing_Township,_Mercer_County,_New_Jersey.jpg}. \\
      Thunder picture under CC-BY-SA 4.0,
      \url{https://commons.wikimedia.org/wiki/File:004_2018_05_14_Extremes_Wetter.jpg}.
      \\[1.5mm]
      Copyright of other images included in this document is held by
      their respective owners.
      \\[3.0mm]
      This work is licensed under the \alert{Creative Commons
        Attribution-Share Alike 3.0} License.  To view a copy of this
      license, visit
      \url{http://creativecommons.org/licenses/by-sa/3.0/} or send a
      letter to Creative Commons, 171 Second Street, Suite 300, San
      Francisco, California, 94105, USA.
      \\[2.0mm]
      At your option, you may instead copy, distribute and/or modify
      this document under the terms of the \alert{GNU Free Documentation
        License, Version 1.3 or any later version} published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts, and no Back-Cover Texts.  A copy of the license is
      available at \url{http://www.gnu.org/licenses/gfdl.html}.
      \\[2.0mm]
      % Give a link to the 'Transparent Copy', as per Section 3 of the GFDL.
      The source of this document is available from
      \url{http://git.sv.gnu.org/cgit/guix/maintenance.git}.
    }
  \end{textblock}
\end{frame}

\end{document}

% Local Variables:
% coding: utf-8
% comment-start: "%"
% comment-end: ""
% ispell-local-dictionary: "american"
% compile-command: "rubber --pdf talk.tex"
% End:
