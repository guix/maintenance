Guix Containers for HPC
=======================
|

.. figlet:: I   don't   want   THAT   running   on   MY   server !

About me
========
|

    * Guix contributor since 2015
    * Work with the University of Tennessee
    * Based on a true story
    * I'm here representing myself

Based on actual events
======================
.. hidetitle::
.. cowsay:: NO SCIENTISTS WERE INJURED FOR THIS PRESENTATION OR FOR YOUR ENJOYMENT
.. figlet:: I   PROMISE

Pithy Title
===========
.. hidetitle::

| I was working from home quietly as I often do and boss-man
| tells me we have a new webapp we want to host on the server.

THE NEW WEBAPP
==============
|

    * It uses PHP, Python and Octave
    * PHP

      * The server it's running on now has php-5.3.3
      * Released back in July 2011
      * 5.3.x EOL since July 2016
    * Python

      * It's using Python-2.4
      * And another Python from the user's home directory
    * Octave

      * From 2010


Oh, one more thing...
=====================
|

.. cowsay:: it accepts arbitrary files from users on the Internet

First some things about Guix
============================
|
| Functional Package Management:
|
|   Each package depends on the specific packages
|   it's built against. Changing any one of them
|   results in a new package.

.. cowsay:: I'm the sum-total of all my cow ancestors.

First some things about Guix
============================
|
| Functional Profile Management:
|
|   The profile of packages gets rebuilt on each change.

.. cowsay:: It's like someone straightens the grass I previously trampled right before I come out of the barn.

First some things about Guix
============================
|
|  Functional System Management:
|
|   You can declare a whole operating system
|   (or container!)
|   which does not depend on previous iterations.

.. cowsay:: Help! I'm a cow in a box! Completely different than the last time I was in a box!

Back to the new webapp
======================
|

    * It uses PHP, Python and Octave
    * PHP

      * The server it's running on now has php-5.3.3
      * Released since July 2011
      * 5.3.x EOL since July 2016
    * Python

      * It's using Python-2.4
      * And another Python from the user's home directory
    * Octave

      * From 2010

Nothing inside that you didn't intend
=====================================
|

.. code::

    WARNING: [pool www] child 26 said into stderr: "./run.sh: line 19: rm: command not found"
    WARNING: [pool www] child 26 said into stderr: "./run.sh: line 20: rmdir: command not found"
    WARNING: [pool www] child 26 said into stderr: "sh: ./run_scripts/run_octave: Permission denied"
    WARNING: [pool www] child 27 said into stderr: "./run.sh: line 2: dirname: command not found"
    WARNING: [pool www] child 27 said into stderr: "./run.sh: line 7: mkdir: command not found"

Time to build some packages like its 2010
=========================================
|
|

.. code::

    @@@@@@@@@@@@@@@@@       @@@@@@@@@@@@@@@@@
    @               @       @               @
    @ python2-2.4.6 @       @   glpk-4.48   @
    @               @       @               @
    @@@@@@@@@@@@@@@@@       @@@@@@@@@@@@@@@@@
            |                       |
            |                       |
            |                       |
           \|/                     \|/
    @@@@@@@@@@@@@@@@@       @@@@@@@@@@@@@@@@@
    @               @       @               @
    @ graphviz-2.26 @       @ octave-3.4.3  @
    @               @       @               @
    @@@@@@@@@@@@@@@@@       @@@@@@@@@@@@@@@@@
                \               /
                 \             /
                  \|         |/
               @@@@@@@@@@@@@@@@@
               @               @
               @      BNW      @
               @               @
               @@@@@@@@@@@@@@@@@

GRAPHVIZ-2.26
=============
|

.. code:: scheme

    (define-public graphviz-2.26
      (package
        (inherit graphviz)
        (name "graphviz")
        (version "2.26.3")
        (outputs '("out"))
        (source
          (origin
            (method url-fetch)
            (uri (string-append
                   ;; TODO: Replace with official release
                   "mirror://debian/pool/main/g/graphviz/"
                   "graphviz_" version ".orig.tar.gz"))
            (file-name (string-append name "-" version ".tar.gz"))
            (sha256
             (base32
              "18bzyg17ni0lpcd2g5dhan8fjv3vzkjym38jq8vm42did5p9j47l"))))
        ;; TODO: unbundle libraries?

GRAPHVIZ-2.26
=============
|

.. code:: scheme

        (arguments
         `(#:configure-flags '("--enable-python24-yes")
           ,@(substitute-keyword-arguments (package-arguments graphviz)
               ((#:phases phases)
                `(modify-phases ,phases
                   (delete 'move-docs)
                   (delete 'move-guile-bindings))))))
        (inputs
          ;; TODO: Add(?) perl, guile@1.8, gtk@2, lua5.1, tcl8.[3-6], rsvg
         `(("gdk-pixbuf" ,gdk-pixbuf)
           ("freeglut" ,freeglut)
           ("python2.4" ,python-2.4)
           ,@(fold alist-delete (package-inputs graphviz)
                   (list "libjpeg" "guile"))))
        (license license:cpl11.0)))

OCTAVE-3.4.3
============
|

.. code:: scheme

    (define-public octave-3.4.3
      (package
        (inherit octave-cli)
        (name "octave")
        (version "3.4.3")
        (source
          (origin
            (method url-fetch)
            (uri (string-append "mirror://gnu/octave/octave-"
                                version ".tar.gz"))
            (sha256
             (base32
              "04lh3crzwpramvfvxq34n2r29pmfl37rssj853nz9a3pyq7wrrir"))
            (patches (search-patches "gnulib-gets.patch"
                                     "octave-nested-class.patch"))))

OCTAVE-3.4.3
============
|

.. code:: scheme

        (arguments
         (substitute-keyword-arguments (package-arguments octave-cli)
           ((#:configure-flags cf)
            `(cons "--enable-docs=no" ; docs fail to build
                   ,cf))
           ((#:phases phases)
            `(modify-phases ,phases
               (add-after 'unpack 'patch-configure-script
                 (lambda _
                   (substitute* '("configure"
                                  "src/DLD-FUNCTIONS/__delaunayn__.cc"
                                  "src/DLD-FUNCTIONS/__voronoi__.cc"
                                  "src/DLD-FUNCTIONS/convhulln.cc")
                     (("qhull/qhull.h") "libqhull/libqhull.h")
                     (("qhull/qhull_a.h") "libqhull/qhull_a.h"))
                   #t))
               (add-after 'unpack 'update-gnulib
                 (lambda* (#:key inputs #:allow-other-keys)
                   (let ((gnulib (assoc-ref inputs "gnulib")))
                     (install-file (string-append gnulib "/lib/fseeko.c") "libgnu")
                     #t)))
               (replace 'configure-makeinfo
                 (lambda* (#:key inputs #:allow-other-keys)
                   (substitute* "src/help.cc"
                     (("\"makeinfo\"")
                      (string-append
                        "\"" (assoc-ref inputs "texinfo") "/bin/makeinfo\"")))
                   #t))))))

OCTAVE-3.4.3
============
|

.. code:: scheme

        (native-inputs
         `(("gnulib" ,(package-source gnulib))
           ("gcc-5" ,gcc-5)
           ,@(fold alist-delete (package-native-inputs octave-cli)
                   '("lzip"))))
        (inputs
         `(("glpk" ,glpk-4.48)
           ("gperf" ,gperf)
           ,@(fold alist-delete (package-inputs octave-cli)
                   ;; suitesparse provides cholmod_common_struct,
                   ;; may need older version
                   '("glpk" "suitesparse"))))))

Lets create a Guix container file
=================================
|

.. code:: scheme

    (operating-system
      (host-name "bnw")
      (timezone "Etc/UTC")
      (locale "en_US.utf8")

      (bootloader (bootloader-configuration
                   (bootloader grub-bootloader)
                   (target "does-not-matter")))
      (file-systems %base-file-systems)
      ;; No firmware for VMs.
      (firmware '())
      ;; We don't need any packages inside the container.
      (packages '())

      (services (list (service dhcp-client-service-type)
                      (service bnw-service-type
                               ;; Uncomment for testing:
                               ;(bnw-configuration
                               ;  (port '("8888")))
                               ))))

Defining the service
====================
|

.. code:: scheme

    (define-record-type* <bnw-configuration>
      bnw-configuration
      make-bnw-configuration
      bnw-configuration?
      (package          bnw-configuration-package       ; package
                        (default bnw))
      (deploy-directory bnw-deploy-directory            ; string
                        (default "/srv/http"))
      (port             bnw-configuration-port          ; list of strings
                        (default '("8880"))))

    (define bnw-activation
      (match-lambda
        (($ <bnw-configuration> package deploy-directory port)
         #~(begin
             (use-modules (guix build utils))
             (when (directory-exists? #$deploy-directory)
               ;; Not 'delete-file-recursively' because the directory might be empty.
               (invoke (file-append #$coreutils "/bin/rm") "-r"
                 (string-append #$deploy-directory "/*")))
             (mkdir-p #$deploy-directory)
             (copy-recursively #$package #$deploy-directory)
             (invoke (file-append #$coreutils "/bin/chmod") "a+w"
                     (string-append #$deploy-directory "/sourcecodes/data"))))))

Defining the service
====================
|

.. code:: scheme

    (define bnw-nginx-config
      (match-lambda
        (($ <bnw-configuration> package deploy-directory port)
         (list
           (nginx-server-configuration
             (server-name '("Bayesian Network"))
             (listen port)
             (root deploy-directory)
             (locations
               (list (nginx-php-location))))))))

    (define bnw-service-type
      (service-type
        (name 'bnw)
        (extensions
          (list
            (service-extension activation-service-type
                               bnw-activation)
            (service-extension nginx-service-type
                               bnw-nginx-config)
            ;; Make sure BNW doesn't get garbage collected.
            (service-extension profile-service-type
                               (compose list bnw-configuration-package))
            ;; Make sure php-fpm is instantiated.
            (service-extension php-fpm-service-type
                               (const #t))))
        (default-value (bnw-configuration))
        (description "Run a Bayesian Network Webserver.")))

Then we build the container
===========================
|

.. code:: ini

    guix system container bnw-container.scm \
    --share=/home/bnw/server=/srv/http \
    --share=/home/bnw/var-log=/var/log \
    --network

Use systemd to launch the app
=============================
|

.. code:: ini

   [Unit]
   Description=BNW web server
   Wants=guix-daemon.service
   [Service]
   Environment="BNW_COMMAND=$(/bin/su -l bnw -c \
    '/var/guix/profiles/per-user/bnw/current-guix/bin/guix system container \
    /home/bnw/guix-bioinformatics/gn/services/bnw-container.scm \
    --share=/home/bnw/server=/srv/http \
    --share=/home/bnw/var-log=/var/log \
    --network')"
   ExecStart=/bin/bash -c '${BNW_COMMAND}'
   [Install]
   WantedBy=multi-user.target

|
| And to update the webapp:

.. code:: shell

   $ guix pull
   # sudo systemctl restart bnw.service

END SLIDE
=========
.. hidetitle::
.. figlet:: QUESTIONS?

| https://hpc.guix.info/
| https://guix.gnu.org/
|
| #guix on irc.freenode.net
| guix-devel@gnu.org
| help-guix@gnu.org
|
| efraim@flashner.co.il
