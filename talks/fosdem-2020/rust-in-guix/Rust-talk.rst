Packaging Rust crates in GNU Guix
=================================
|

.. figlet:: How   hard   could   it   possibly   be?

About me
========
|

    * Guix contributor since 2015
    * First Rust related commit December 2017
    * Primarily interested in distro maintenance

      * Watching packages compile is soothing

Quick overlapping history of Rust and GNU Guix
==============================================
|

    * First stable Rust release May 2015
    * Crate importer added in December 2016

      * (guix import crate foo)
    * 'Blessed Bootstrap Binaries' and 'rustc' package added in December 2016

      * i686-linux and x86_64-linux only
      * version 1.12.1
      * 'cargo-build-system' added
    * 'cargo' added in April 2017.
    * 'Blessed Bootstrap Binaries' added for all systems supported in Guix.

      * December 2017
      * (This was me, I like when packages build)
    * 'mrustc' added in April 2018.
    * Use 'mrustc' for bootstrap path, September 2018

      * 'Blessed Bootstrap Binaries' removed.
    * Add rust to icecat build, September 2018. Icecat-60.2.0
    * First crate added, rust-unicode-xid. April 2019.
    * First 'rust app', rust-cbindgen, added October 2019. Used for Icecat-68.

Looking at a standard package
=============================
|

.. code:: shell

    tree $(guix build sqlite)

|

.. code::

    /gnu/store/i6l1579g80387rda658jy9cfqq82643d-sqlite-3.28.0
    |-- bin
    |   `-- sqlite3
    |-- include
    |   |-- sqlite3.h
    |   `-- sqlite3ext.h
    |-- lib
    |   |-- libsqlite3.a
    |   |-- libsqlite3.la
    |   |-- libsqlite3.so -> libsqlite3.so.0.8.6
    |   |-- libsqlite3.so.0 -> libsqlite3.so.0.8.6
    |   |-- libsqlite3.so.0.8.6
    |   `-- pkgconfig
    |       `-- sqlite3.pc
    `-- share
        `-- man
            `-- man1
                `-- sqlite3.1.gz

Looking at a rust package
=========================
|

.. code:: shell

    tree $(guix build rust-rand@0.6)

|

.. code::

    /gnu/store/gx7lr6c45irpig0cy1wc36gasfh3yfic-rust-rand-0.6.5
    `-- share
        `-- doc
            `-- rust-rand-0.6.5
                |-- LICENSE-APACHE
                `-- LICENSE-MIT

The Crate.toml
==============
|

.. code:: toml

    [package]
    name = "rand"
    version = "0.6.5"
    authors = ["The Rand Project Developers", "The Rust Project Developers"]
    license = "MIT/Apache-2.0"
    readme = "README.md"
    repository = "https://github.com/rust-random/rand"
    documentation = "https://rust-random.github.io/rand"
    homepage = "https://crates.io/crates/rand"
    description = """
    Random number generators and other randomness functionality.
    """
    keywords = ["random", "rng"]
    categories = ["algorithms", "no-std"]
    build = "build.rs"
      ...
    [dependencies]
    rand_core = { path = "rand_core", version = "0.4" }
    rand_pcg = { path = "rand_pcg", version = "0.1" }
    rand_jitter = { path = "rand_jitter", version = "0.1" }
    rand_os = { path = "rand_os", version = "0.1", optional = true  }
    # only for deprecations and benches:
    rand_isaac = { path = "rand_isaac", version = "0.1" }
    rand_chacha = { path = "rand_chacha", version = "0.1" }
    rand_hc = { path = "rand_hc", version = "0.1" }
    rand_xorshift = { path = "rand_xorshift", version = "0.1" }
    log = { version = "0.4", optional = true }

Let's hit the API
=================
|

.. code:: shell

    curl https://crates.io/api/v1/crates/rand/0.6.5

|

.. code:: json

    {"version":
      {"id":130460,
       "crate":"rand",
       "num":"0.6.5",
       "dl_path":"/api/v1/crates/rand/0.6.5/download",
       "readme_path":"/api/v1/crates/rand/0.6.5/readme",
       "updated_at":"2019-01-28T09:56:57.788327+00:00",
       "created_at":"2019-01-28T09:56:57.788327+00:00",
       "downloads":4211330,
       "features":
        {"alloc":["rand_core/alloc"],
         "default":["std"],
         "i128_support":[],
         "nightly":["simd_support"],
         "serde1":["rand_core/serde1", "rand_isaac/serde1", "rand_xorshift/serde1"],
         "simd_support":["packed_simd"],
         "std":["rand_core/std", "alloc", "rand_os", "rand_jitter/std"],
         "stdweb":["rand_os/stdweb"],
         "wasm-bindgen":["rand_os/wasm-bindgen"]},
       "yanked":false,
       "license":"MIT/Apache-2.0",
       "links":
        {"dependencies":"/api/v1/crates/rand/0.6.5/dependencies",
         "version_downloads":"/api/v1/crates/rand/0.6.5/downloads",
         "authors":"/api/v1/crates/rand/0.6.5/authors"},
       "crate_size":104814,
       "published_by":null,
       "audit_actions":[]}}

Let's hit the API
=================
|

.. code:: shell

    curl https://crates.io/api/v1/crates/rand/0.6.5/dependencies

|

.. code:: json

    {"dependencies":[
      {"id":625191,"version_id":130460,"crate_id":"autocfg","req":"^0.1",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"build","downloads":0},
      {"id":625189,"version_id":130460,"crate_id":"average","req":"^0.9.2",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"dev","downloads":0},
      {"id":625192,"version_id":130460,"crate_id":"libc","req":"^0.2",
       "optional":false,"default_features":false,"features":[],"target":"cfg(unix)","kind":"normal","downloads":0},
      {"id":625181,"version_id":130460,"crate_id":"rand_chacha","req":"^0.1",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"normal","downloads":0},
      {"id":625182,"version_id":130460,"crate_id":"rand_core","req":"^0.4",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"normal","downloads":0},
      {"id":625183,"version_id":130460,"crate_id":"rand_hc","req":"^0.1",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"normal","downloads":0},
      {"id":625184,"version_id":130460,"crate_id":"rand_isaac","req":"^0.1",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"normal","downloads":0},
      {"id":625185,"version_id":130460,"crate_id":"rand_jitter","req":"^0.1",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"normal","downloads":0},
      {"id":625187,"version_id":130460,"crate_id":"rand_pcg","req":"^0.1",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"normal","downloads":0},
      {"id":625188,"version_id":130460,"crate_id":"rand_xorshift","req":"^0.1",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"normal","downloads":0},
      {"id":625190,"version_id":130460,"crate_id":"rand_xoshiro","req":"^0.1",
       "optional":false,"default_features":true,"features":[],"target":null,"kind":"dev","downloads":0},
      {"id":625193,"version_id":130460,"crate_id":"winapi","req":"^0.3",
       "optional":false,"default_features":true,"features":["minwindef","ntsecapi","profileapi","winnt"],"target":"cfg(windows)","kind":"normal","downloads":0},
      {"id":625179,"version_id":130460,"crate_id":"log","req":"^0.4",
       "optional":true,"default_features":true,"features":[],"target":null,"kind":"normal","downloads":0},
      {"id":625180,"version_id":130460,"crate_id":"packed_simd","req":"^0.3",
       "optional":true,"default_features":true,"features":["into_bits"],"target":null,"kind":"normal","downloads":0},
      {"id":625186,"version_id":130460,"crate_id":"rand_os","req":"^0.1",
       "optional":true,"default_features":true,"features":[],"target":null,"kind":"normal","downloads":0}]}


A crate package definition:
===========================
|

.. code:: shell

    guix import crate rand@0.6.5

.. code:: scheme

  (define-public rust-rand-0.6
    (package
      (name "rust-rand")
      (version "0.6.5")
      (source ...)
      (build-system cargo-build-system)
      (arguments
       `(#:cargo-inputs
         (("rust-autocfg" ,rust-autocfg-0.1)
          ("rust-libc" ,rust-libc-0.2)
          ("rust-log" ,rust-log-0.4)
          ("rust-packed-simd" ,rust-packed-simd-0.3)
          ("rust-rand-chacha" ,rust-rand-chacha-0.1)
          ("rust-rand-core" ,rust-rand-core-0.4)
          ("rust-rand-hc" ,rust-rand-hc-0.1)
          ("rust-rand-isaac" ,rust-rand-isaac-0.1)
          ("rust-rand-jitter" ,rust-rand-jitter-0.1)
          ("rust-rand-os" ,rust-rand-os-0.1)
          ("rust-rand-pcg" ,rust-rand-pcg-0.1)
          ("rust-rand-xorshift" ,rust-rand-xorshift-0.1)
          ("rust-winapi" ,rust-winapi-0.3))
         #:cargo-development-inputs
         (("rust-average" ,rust-average-0.9)
          ("rust-rand-xoshiro" ,rust-rand-xoshiroi-0.1))))
      (home-page "https://crates.io/crates/rand")
      (synopsis "Random number generators and other randomness functionality.")
      (description "Random number generators and other randomness functionality.")
      (license (list license:expat license:asl2.0))))

A "normal" package definition:
==============================
|

.. code:: scheme

    (define-public newsboat
      (package
        (name "newsboat")
        (version "2.13")
        (source ...)
        (build-system gnu-build-system)
        (native-inputs
         `(("gettext" ,gettext-minimal)
           ("pkg-config" ,pkg-config)
           ("asciidoc" ,asciidoc))) ; For building documentation.
        (inputs
         `(("curl" ,curl)
           ("json-c" ,json-c)
           ("libxml2" ,libxml2)
           ("ncurses" ,ncurses)
           ("stfl" ,stfl)
           ("sqlite" ,sqlite)))
        (arguments
         '(#:phases
           (modify-phases %standard-phases
             (delete 'configure)            ; no configure script
             (add-after 'build 'build-documentation
               (lambda _ (invoke "make" "doc"))))
           #:make-flags (list (string-append "prefix=" (assoc-ref %outputs "out")))
           #:test-target "test"))
        (native-search-paths ; Newsboat respects CURL_CA_BUNDLE.
         (package-native-search-paths curl))
        (home-page "https://newsboat.org/")
        (synopsis "Text-mode RSS and Atom feed reader with podcast support")
        (description ...)
        (license (list license:gpl2+        ; filter/*
                       license:expat))))    ; everything else

Cargo inputs vs Inputs
======================
.. hidetitle::
.. figlet::
    CARGO   INPUTS   VS .   INPUTS

|

.. cowsay::
    what's the difference?

Shared library vs Shared source
===============================
|

.. figlet:: THIS   IS   NOT   ABOUT   VENDORED   CODE

Shared library vs Shared source
===============================
|

    * Shared library

      * Compiled once
      * Linked to many packages
    * Shared source

      * Complied each time
      * Inlined into each package

|

    * Vendored code

      * Bundled for each package
      * Rarely updated

The Build Directory
===================
|

.. code:: shell

    $ ls /tmp/guix-build-rust-serde-1.0.104.drv-0/serde-1.0.104/target/release/deps/ -l
    total 31132
    -rw-r--r-- 1 efraim users 1018326 Jan 24 12:59 libproc_macro2-7bef2a1402ddd599.rlib
    -rw-r--r-- 1 efraim users  208633 Jan 24 12:59 libproc_macro2-7bef2a1402ddd599.rmeta
    -rw-r--r-- 1 efraim users  550366 Jan 24 12:59 libquote-c787bef5732c5c12.rlib
    -rw-r--r-- 1 efraim users  153523 Jan 24 12:59 libquote-c787bef5732c5c12.rmeta
    -rw-r--r-- 2 efraim users 8622032 Jan 24 12:59 libserde-94f3d254a5e60c6a.rlib
    -rw-r--r-- 1 efraim users 8351721 Jan 24 12:59 libserde-94f3d254a5e60c6a.rmeta
    -rwxr-xr-x 1 efraim users 4501960 Jan 24 13:00 libserde_derive-98f01a3566b7b6f3.so
    -rw-r--r-- 1 efraim users 5497364 Jan 24 12:59 libsyn-85ba4f4d661df8a9.rlib
    -rw-r--r-- 1 efraim users 1731518 Jan 24 12:59 libsyn-85ba4f4d661df8a9.rmeta
    -rw-r--r-- 1 efraim users  185702 Jan 24 12:59 libunicode_xid-a72a6925758cfc76.rlib
    -rw-r--r-- 1 efraim users  161597 Jan 24 12:59 libunicode_xid-a72a6925758cfc76.rmeta
    -rw-r--r-- 1 efraim users    2098 Jan 24 12:59 proc_macro2-7bef2a1402ddd599.d
    -rw-r--r-- 1 efraim users    3263 Jan 24 12:59 quote-c787bef5732c5c12.d
    -rwxr-xr-x 2 efraim users  829760 Jan 24 13:01 serde-67a50562fd1aa3b9
    -rw-r--r-- 1 efraim users    1110 Jan 24 13:00 serde-67a50562fd1aa3b9.d
    -rw-r--r-- 1 efraim users    1521 Jan 24 12:58 serde-94f3d254a5e60c6a.d
    -rw-r--r-- 1 efraim users    5457 Jan 24 12:59 serde_derive-98f01a3566b7b6f3.d
    -rw-r--r-- 1 efraim users   15563 Jan 24 12:59 syn-85ba4f4d661df8a9.d
    -rw-r--r-- 1 efraim users    1204 Jan 24 12:59 unicode_xid-a72a6925758cfc76.d


Can an rlib be reused?
======================
|

.. code:: scheme

    (replace 'build
      (lambda* (#:key inputs #:allow-other-keys)
        (let ((ndarray    (assoc-ref inputs "ndarray"))
              (rand       (assoc-ref inputs "rand"))
              (rayon      (assoc-ref inputs "rayon"))
              (serde      (assoc-ref inputs "serde"))
              (serde-json (assoc-ref inputs "serde-json"))
              (structopt  (assoc-ref inputs "structopt")))
          (invoke "rustc" "--edition=2018" "--crate-name" "qtlreaper"
                  "src/lib.rs" "--crate-type" "lib"
                  "-C" "opt-level=3"
                  "--out-dir" "target/release/deps"
                  "--extern" (string-append "ndarray=" ndarray "/lib/libndarray.rlib")
                  "--extern" (string-append "rand=" rand "/lib/librand.rlib")
                  "--extern" (string-append "rayon=" rayon "/lib/librayon.rlib")
                  "--extern" (string-append "serde=" serde "/lib/libserde.rlib")
                  "--extern" (string-append "serde_json=" serde-json "/lib/libserde_json.rlib")
                  "--extern" (string-append "structopt=" structopt "/lib/structopt.rlib")
                  "--cap-lints" "allow" "-C" "rpath" "-C" "prefer-dynamic")
          (invoke "rustc" "--edition=2018" "--crate-name" "qtlreaper"
                  "src/main.rs" "--crate-type" "bin"
                  "-C" "opt-level=3"
                  "--out-dir" "target/release/deps"
                  "--extern" "qtlreaper=target/release/deps/libqtlreaper.rlib"
                  "--extern" (string-append "ndarray=" ndarray "/lib/libndarray.rlib")
                  "--extern" (string-append "rand=" rand "/lib/librand.rlib")
                  "--extern" (string-append "rayon=" rayon "/lib/librayon.rlib")
                  "--extern" (string-append "serde=" serde "/lib/libserde.rlib")
                  "--extern" (string-append "serde_json=" serde-json "/lib/libserde_json.rlib")
                  "--extern" (string-append "structopt=" structopt "/lib/structopt.rlib")
                  "--cap-lints" "allow" "-C" "rpath" "-C" "prefer-dynamic"))))

END SLIDE
=========
.. hidetitle::
.. figlet:: QUESTIONS?

| https://guix.gnu.org/
|
| #guix on irc.freenode.net
| guix-devel@gnu.org
| help-guix@gnu.org
|
| efraim@flashner.co.il
