* Branches / Releases / Build farm

** Problems

 - One core updates (GCC 5 to GCC 7), broke, then got fixed after the
   merge

 - No Guix release with time-machine

 - Installing from the installation image, GCC update, nss-certs
   embeds characters, locales cause substitutes to fail

   - Install
   - Do a pull
   - Reconfigure fails

 - A long time ago, a bug was introduced in to Guix, the guix package
   was updated, this broke the installation image

 - What's the point of having releases?
   - Attracting new users

 - Build servers
   - Substitute availability
     - Uncertian how good this is
   - Testing reproducible builds
   - Redundancy
   - All in Europe/Networking

** Actions

*** TODO Find or file bug for nss-certs failure
*** TODO Publishing release artefacts nightly

*** TODO Consider time of year release schedule

 - Rough objective

*** TODO Prioritise package builds

 - By NGinx logs for nar requests
 - By build time

*** TODO Gather statistics on the build process

 - Build time
 - Resource usage

*** TODO Provide tooling to easily continuously and automatically build packages ("Derivations at home")

