Staging in GNU Guix
=================

GNU Guix is a “functional” package manager that borrows from earlier
work on Nix by Dolstra _et al._.  Guix implements high-level
abstractions such as packages and operating system services as
domain-specific languages (DSL) embedded in Scheme, and it also
implements build actions and operating system orchestration in Scheme.
This leads to a multi-tier programming environment where embedded code
snippets are _staged_ for eventual execution.

In this paper we present _G-expressions_ or “_gexps_”.  We explain our
journey from traditional Lisp S-expressions to G-expressions, which
augment the former with contextual information, and we discuss the
implementation of gexps.  We report on our experience using gexps in a
variety of operating system use cases—from package build processes, to
initial RAM disk code, to system services.  To our knowledge, gexps
provide a unique way to cover many aspects of OS configuration in a
single, multi-tier language, and to facilitate code reuse and code
sharing.  Finally we compare to related work on staging.
