#~(begin
    (use-modules (gnu build marionette)
                 (srfi srfi-64) (ice-9 match))

    ;; Spawn the VM that runs the declared OS.
    (define marionette (make-marionette (list #$vm)))

    (test-begin "basic")
    (test-assert "uname"
      (match (marionette-eval '(uname) marionette)
        (#("Linux" host-name version _ architecture)
         (and (string=? host-name
                        #$(operating-system-host-name os))
              (string-prefix? #$(package-version
                                 (operating-system-kernel os))
                              version)
              (string-prefix? architecture %host-type)))))
    (test-end)
    (exit (= (test-runner-fail-count (test-runner-current)) 0))))
