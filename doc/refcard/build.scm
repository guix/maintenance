;; Build file for the GNU Guix Reference Card.
;; Run "guix build -f build.scm".
;;
;; Copyright © 2018, 2019 Ludovic Courtès <ludo@gnu.org>
;; Released under the GNU GPLv3 or any later version.

(use-modules (guix) (gnu)
             (guix git-download)
             (guix build-system trivial)
             ((guix licenses) #:prefix license:)
             (gnu packages tex))

(define ghostscript (specification->package "ghostscript"))
(define lout (specification->package "lout"))
(define font-gentium (specification->package "font-sil-gentium"))
(define font-charis (specification->package "font-sil-charis"))
(define texlive-fonts-lm (specification->package "texlive-lm"))
(define coreutils (specification->package "coreutils"))
(define ttf2pt1 (specification->package "ttf2pt1"))

(define (truetype->type1 fonts)
  "Return a directory containing Type 1 .afm and .pfa files for FONTS."
  (define build
    (with-imported-modules '((guix build utils))
      #~(begin
          (use-modules (guix build utils)
                       (srfi srfi-1))

          (define ttf-files
            (append-map (lambda (font)
                          (find-files font "\\.ttf$"))
                        '#$fonts))

          (define directory
            (string-append #$output "/share/fonts/type1"))

          (mkdir-p directory)
          (for-each (lambda (ttf)
                      (let ((base (string-append directory "/"
                                                 (basename ttf ".ttf") )))
                        (invoke #$(file-append ttf2pt1 "/bin/ttf2pt1")
                                "-e" ttf base)))
                    ttf-files))))

  (computed-file "type1-fonts" build))

(define* (lout->pdf directory file #:key
                    (fonts (list font-gentium font-charis)))
  "Build Lout source FILE, taken from DIRECTORY, and return the resulting
PDF."
  (define font-directory
    (truetype->type1 fonts))

  (define build
    (with-imported-modules '((guix build utils))
      #~(begin
          (use-modules (guix build utils))

          (define ps-file
            (string-append #$output "/"
                           #$(basename file ".lout") ".ps"))

          (define pdf-file
            (string-append #$output "/"
                           #$(basename file ".lout") ".pdf"))

          (mkdir #$output)
          (copy-recursively #$directory ".")
          (invoke #$(file-append lout "/bin/lout") "-a" "-r3"
                  "-I."
                  "-F" #$(file-append font-directory "/share/fonts/type1")
                  "-F" #$(file-append texlive-lm
                                      "/share/texmf-dist/fonts/afm/public/lm")
                  "-s" #$file "-o" ps-file)

          (setenv "PATH" (string-join '(#$ghostscript #$coreutils)
                                      "/bin:" 'suffix))
          (setenv "GS_FONTPATH"
                  (string-append #$font-directory "/share/fonts/type1:"
                                 #$texlive-lm
                                 "/share/texmf-dist/fonts/type1/public/lm"))
          (invoke #$(file-append ghostscript "/bin/ps2pdf")
                  "-dPDFSETTINGS=/prepress" "-sPAPERSIZE=a4"
                  ps-file pdf-file))))

  (computed-file (basename file ".lout") build))

(define this-directory
  (local-file (dirname (assoc-ref (current-source-location) 'filename))
              "guix-refcard-source"
              #:recursive? #t
              #:select? (git-predicate ".")))

(lout->pdf this-directory "guix-refcard.lout")
