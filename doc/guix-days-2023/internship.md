# Internships, Outreachy, GSoC et al

## Motivation
* prevent Guix becoming a niche project
* attracting new people, increase audience...

## Internships
* GSoC isn't financally attractive for students as in the past
* doing our own summer of code -> funding is available
* successes of the past are a bit forgotten -> some where extremly successful
* GNU project no longer an umbrella organization -> we need to apply for ourself
* organizations can apply for GSoC 2023 until 2023-02-07 -> we don't do it this year

## Outreachy
* winter 2023/24
* problem: Outreachy doesn't accept funding by FSF (RMS story)
* need to find a way to do it via Guix Europe -> Andreas could help
* we need mentors
* we need projects
* coordinator for overall organizational stuff

## Ideas
* have more than one internship at the same time -> student review code of each other
* dedicated IRC channels for intership as done in the past
* https://libreplanet.org/wiki/Group:Guix/GSoC-2021
* having more then one mentor for a student
* paying the mentor (GSoC gives 500$ per mentor)
* project should be "small" enough to upstream it during the internship time period

## How to make it resilient?
* have post-internship "mentor ship"
* offer some "gift" in the aftermath -> e.g. 1 year of a small server for running Guix System
* transparently communicate what's going on to the Guix community

## Action plan
* contacting Google (Gabor)
* contacting Outreachy (Gabor)
* arrangement with Outreachy about the funding (Gabor, Andreas)
* check requirements of GSoC (Gabor)
* process description (Gabor)
* start communication on the mailing list and IRC
* define fall back contacts
