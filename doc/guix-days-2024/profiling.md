# What is profiling?

- Have an idea of the execution time of part of code
- Have some kind of flame graph
- What feels slow (not necessarily what is slow)
- Where's RAM most used?

# What should be profiled?

- guix authenticate
- guix system/home
- guix pull
- not the compilation itself
- Guix system start time (like ~systemd-analyse~)
- Time of deployments

# Why?

- To be able to use Guix on low-end/older devices
- Building a package is long (because of compilation)
- Pulling is slow (because of network)
- Computing Guix derivations (guix system init/reconfigure)
- Authenticating guix commits (especially with many/big commits)

- It's compelling (for HPC people for instance)

- To know where there's progress to be made
- ~make~ in the Guix repo is slow so testing is not as immediate as it could be
- As small/fast finale binary as possible
- To get a feel of what specs are needed to do actual stuff

# How?

- Guile has ~trace~ / ~profile~ procedure that you could use on Guix procs
  - How to make a flame graph from this data or any other format?
- Retrieving some metrics on demand from build systems (via ~guix build~)
  - to get logs RAM/space/temporal usage during compilation
- Profiling daemons resources usage from shepherd (exposing those profiling options to users)
- Making shepherd services to profile ~guix shell --container~

# Starting point

```
$ guix repl
(guix-user)> ,use (guix scripts build)
(guix-user)> ,profile (guix-build "hello")
%     cumulative   self             
time   seconds     seconds  procedure
 24.00      0.16      0.16  put-string
 16.00      0.11      0.11  write
 12.00      0.11      0.08  append
 12.00      0.08      0.08  get-bytevector-n
  4.00      0.05      0.03  guix/gexp.scm:809:0:gexp-attribute
  4.00      0.03      0.03  string->utf8
  4.00      0.03      0.03  guix/packages.scm:1969:0:package->derivation
  4.00      0.03      0.03  regexp-exec
  4.00      0.03      0.03  cons
  4.00      0.03      0.03  guix/store.scm:1426:4
  4.00      0.03      0.03  apply-smob/1
  4.00      0.03      0.03  sort
(guix-user)> ,trace (guix-build "hello") ; doesn't output any trace for some reason
/gnu/store/6fbh8phmp3izay6c0dpggpxhcjn4xlm5-hello-2.12.1
(guix-user)> ^D
$ guile
(guile-user)> ,use (guix scripts build)
(guile-user)> ,trace (guix-build "hello") ; however it does work
trace: |  (guix-build "hello")
trace: |  |  (_ (guix scripts) #:ensure #f)
trace: |  |  (_ #<procedure 7ff4802c5000 at ice-9/boot-9.scm:3241:7 ()>)
trace: |  |  |  (lock-mutex #<mutex 7ff487d01fc0>)
...
trace: |  |  |  |  |  |  (fileno #<output: file /dev/pts/3>)
trace: |  |  |  |  |  |  1
trace: |  |  |  |  |  |  (bytevector->pointer@system/foreign #vu8(0 0 0 0 0 0 0 0))
trace: |  |  |  |  |  |  (bytevector->pointer #vu8(0 0 0 0 0 0 0 0))
trace: |  |  |  |  |  |  #<pointer 0x7ff48036a7d0>
```

```sh
$ guix build --verbosity=<big number> --debug=<big number>
```
