# Goblins, Hoot & Guix

## Goblins demo

- Date: Thursday, 1st February 2024
- Speaker: Christine Lemmer-Webber

- Christine introduced Goblins, gave a demo and discussed current goals
- Outlined a vision for how Goblins distributed object programming environment and Guix can work together.

## Goblins, Hoot & Guix

- Date, Friday, 2nd February 2024
- Co-ordinator: Christine Lemmer-Webber

- Discussion about how the projects can work together
- Notes TBD
