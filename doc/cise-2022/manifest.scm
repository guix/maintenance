(specifications->manifest
 '("rubber"

   "texlive-base"
   "texlive-latex-wrapfig"

   "texlive-microtype"
   "texlive-latex-listings" "texlive-hyperref"

   ;; PGF/TikZ
   "texlive-latex-pgf"

   ;; Additional fonts.
   "texlive-cm-super" "texlive-amsfonts"
   "texlive-inconsolata" "texlive-latex-xkeyval" "texlive-latex-upquote"
   "texlive-times" "texlive-helvetic" "texlive-courier"))
