#+TITLE: Addressing and announcing security issues

This document describes the process to follow when reporting security
issues in Guix.

* Identify the problem and estimate its impact

  This discussion usually happens on the private guix-security@gnu.org
  list.

* Work on a fix or workaround

  This may happen on guix-security, or it could be tracked in the bug
  tracker.

  In general, bringing issues to public scrutiny can help raise
  awareness and find better solutions.

* Publicize bug and patch at bug-guix@gnu.org

  That gives a bug number that can be used to track progress.

  The bug report should mention, in this order:

    1. who’s affected and who’s not (especially Guix System vs. foreign
       distros);
    2. what users need to do to be safe;
    3. what the problem was and how it could be exploited.

  The bug report may contain the patch (bug fix) as an attachment.

* Commit the bug fix

  The commit log of the bug fix should contain the line:

  #+begin_example
    Fixes <https://bugs.gnu.org/NNN>.
  #+end_example

  where NNN is the bug number obtained above.

* Commit a =etc/news.scm= entry as a followup

  The news entry should be a simplified version of the bug report, with
  the understanding that it will be read by users who just upgraded or
  who are about to upgrade (in cases where the upgrade requires
  additional step, such as running =guix system reconfigure=).

* Report the commit ID in the bug tracker

  Once these two commits have been pushed, reply to NNN@debbugs.gnu.org
  giving the commit ID that contains the fix.

* Announce the issue

** Wrote a blog post with the “Security Advisory” tag

   The blog post should roughly the same as the bug report above.  It
   should contain the bug report URL.  Blog posts are available at
   https://git.savannah.gnu.org/cgit/guix/guix-artwork.git/tree/website/posts.

** Send email to info-guix@gnu.org

   The message be again roughly the same as the blog post, as plain
   text, GPG-signed.

** Send email to the oss-security list (optionally)

   If deemed useful, email the [[https://www.openwall.com/lists/oss-security/][oss-security list]].

* Assign a CVE number via https://cveform.mitre.org/ (?)

  See also https://cve.mitre.org/cve/request_id.html.
