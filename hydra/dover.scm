(use-modules (sysadmin overdrive))

(overdrive-system "dover"
                  #:wireguard-ip "10.0.0.4/32")
