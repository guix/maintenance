(use-modules (sysadmin overdrive)
             (gnu system file-systems))

(operating-system
  (inherit (overdrive-system "lieserl" #:wireguard-ip "10.0.0.14/32"))
  (file-systems (cons* (file-system
                         (device (file-system-label "my-root"))
                         (mount-point "/")
                         (type "btrfs"))
                       (file-system
                         (device "/dev/sda1")
                         (mount-point "/boot/efi")
                         (type "vfat"))
                       %base-file-systems))
    (swap-devices
      (list
        (swap-space
          (target "/dev/sda3")))))
