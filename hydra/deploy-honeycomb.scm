(use-modules (sysadmin honeycomb))

(list
 (machine
  (operating-system
    (honeycomb-system "pankow"
                      #:wireguard-ip "10.0.0.8/32"))
  (environment managed-host-environment-type)
  (configuration
   (machine-ssh-configuration
    (host-name "141.80.167.133")                  ;or 10.0.0.8
    (build-locally? #false)
    (system "aarch64-linux")
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMiOfBwh78K3KNEV1ZQf0pyVtYFSoLgWryMMy0GdMJ0H"))))
 (machine
  (operating-system
    (honeycomb-system "kreuzberg"
                      #:wireguard-ip "10.0.0.9/32"))
  (environment managed-host-environment-type)
  (configuration
   (machine-ssh-configuration
    (host-name "141.80.167.132")                  ;or 10.0.0.9
    (build-locally? #false)
    (system "aarch64-linux")
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOkfbMTdQP0btONZ6CNXsxVQCBaGk1ytNcd4PtIc/53o"))))
 (machine
  (operating-system
    (honeycomb-system "grunewald"
                      #:wireguard-ip "10.0.0.10/32"))
  (environment managed-host-environment-type)
  (configuration
   (machine-ssh-configuration
    (host-name "141.80.167.188")                  ;or 10.0.0.10
    (build-locally? #false)
    (system "aarch64-linux")
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIB9slskCGIBFwRRzsWmePIsMJ8W1muqvDIgPG3xQeu6")))))
