# Database of the build machines and hydra.gnu.org front-end.
#
# The 'PublicKey' field is for /etc/guix/signing-key.pub, the key used
# to sign exported archives.

Nickname: sjd
Hostname: guix.sjd.se
Port: 22
Physical: No
Location: Sweden
Contact: Simon Joseffson <simon@josefsson.org>
SystemType: x86_64-linux
SystemType: i686-linux
Cores: 8
BaseSystem: Debian GNU/Linux 8
MuninURL: https://munin.sjd.se/munin/sjd.se/guix.sjd.se/index.html
PublicKey: (public-key 
+   (ecc 
+    (curve Ed25519)
+    (q #8790B007DC910A372EF541021CD366040D24B6C80356D6A6C561D6DD65455C4B#)
+    )
+   )
+ 

Nickname: x15
Hostname: x15.sjd.se
Port: 22
Physical: Yes
Location: Sweden
Contact: Simon Joseffson <simon@josefsson.org>
SystemType: armhf-linux
Cores: 2
BaseSystem: Debian GNU/Linux 9, BeagleBoard.org Debian Image 2018-09-30
MuninURL: https://munin.sjd.se/munin/sjd.se/x15.sjd.se/index.html
PublicKey: (public-key 
+  (ecc 
+   (curve Ed25519)
+   (q #256BA7A2A9588A36F56EC7437464CAF3C3430CBDFCB35A05189B1F67EF8FDE87#)
+   )
+  )

Nickname: redhill
Hostname: redhill.guix.info
Port: 9022
Physical: Yes
Vendor: Novena
Location: Bordeaux, France
Contact: Andreas Enge <andreas@enge.fr>
Cores: 4
SystemType: armhf-linux
BaseSystem: Debian GNU/Linux Jessie
PublicKey: (public-key
+   (ecc
+    (curve Ed25519)
+    (q #24A48D6C6E144A3B743A7EE6078CB8243BDA2702FD797779F67C172DDD2D89CF#)
+    )
+   )

Nickname: harbourfront
Hostname: harbourfront.guix.info
Port: 22
Physical: Yes
Vendor: ?
Location: Bordeaux, France
Contact: Andreas Enge <andreas@enge.fr>
Cores: 8
SystemType: x86_64-linux
BaseSystem: Guix System
PublicKey: (public-key
+   (ecc
+    (curve Ed25519)
+    (q #ED463268B44B6DC421EA32DF111CBE6A00BC1B0F96071F26B40E695EF4EC8DCB#)
+    )
+   )

Nickname: berlin
Hostname: berlin.guixsd.org
Port: 22
Physical: Yes
Vendor: Dell PowerEdge R7425 with two AMD EPYC 7451 24-Core Processor
Location: Max Delbrück Center, Berlin, Germany
Contact: Ricardo Wurmus <rekado@elephly.net>
Contact: Madalin Patrascu <madalinionel.patrascu@mdc-berlin.de>
Cores: 72
SystemType: x86_64-linux (front-end)
BaseSystem: Guix System, see berlin.scm
PublicKey: (public-key 
+  (ecc 
+   (curve Ed25519)
+   (q #8D156F295D24B0D9A86FA5741A840FF2D24F60F7B6C4134814AD55625971B394#)
+   )
+  )

Nickname: milan-1
Hostname: milano-guix-1.di.unimi.it
Port: 22
Physical: Yes
Vendor: HP, ProLiant DL380p Gen8
Location: Dipartimento di Informatica (DI) UNIMI, Milan, Italy
Contact: Giovanni Biscuolo <g@xelera.eu>
Contact: Andrea Trentini <andrea.trentini@unimi.it>
Cores: 32
SystemType: x86_64-linux
BaseSystem: Guix System, see milano-guix-1.scm
PublicKey: (public-key
+  (ecc
+   (curve Ed25519)
+   (q #6AE1F846DCBE4F3EB01197946F36CAE4A4A5375F51043367BCD53B4E5AA759BD#)
+   )
+  )

Nickname: pankow
Hostname: 10.0.0.8
Port: 22
Physical: Yes
Vendor: SolidRun HoneyComb LX2
Location: MDC data centre in Berlin Buch, Germany
Contact: Ricardo Wurmus <rekado@elephly.net>
Cores: 16
SystemType: aarch64-linux
BaseSystem: Guix System, see modules/sysadmin/honeycomb.scm
PublicKey: (public-key 
+ (ecc 
+  (curve Ed25519)
+  (q #26B3C5D00FF543D3E34FFEB99C4C6DBE1B7904363E0613825F75251D8B8D8B16#)
+  )
+ )

Nickname: kreuzberg
Hostname: 10.0.0.9
Port: 22
Physical: Yes
Vendor: SolidRun HoneyComb LX2
Location: MDC data centre in Berlin Buch, Germany
Contact: Ricardo Wurmus <rekado@elephly.net>
Cores: 16
SystemType: aarch64-linux
BaseSystem: Guix System, see modules/sysadmin/honeycomb.scm
PublicKey: (public-key 
+ (ecc 
+  (curve Ed25519)
+  (q #7E0F021C5657A5DFF0D3FE05B574E16259B28CB477703715A75851E00E95C29E#)
+  )
+ )

Nickname: grunewald
Hostname: 10.0.0.10
Port: 22
Physical: Yes
Vendor: SolidRun HoneyComb LX2
Location: MDC data centre in Berlin Buch, Germany
Contact: Ricardo Wurmus <rekado@elephly.net>
Cores: 16
SystemType: aarch64-linux
BaseSystem: Guix System, see modules/sysadmin/honeycomb.scm
PublicKey: (public-key 
+ (ecc 
+  (curve Ed25519)
+  (q #F73818AF4E6D6F4338B804D5714D24FA6F23B7236CC9EC89D076AF3456D4DBD7#)
+  )
+ )

Nickname: monokuma
Hostname: monokuma.guix.gnu.org
Port: 22
Physical: Yes
Vendor: SoftIron, OverDrive 1000
Location: London, United Kingdom
Contact: Christopher Baines <mail@cbaines.net>
Cores: 4
SystemType: aarch64-linux
BaseSystem: Guix System

Nickname: hatysa
Hostname: hatysa.guix.gnu.org
Port: 22
Physical: Yes
Vendor: SolidRun, HoneyComb LX2
Location: London, United Kingdom
Contact: Christopher Baines <mail@cbaines.net>
Cores: 16
SystemType: aarch64-linux
BaseSystem: Guix System

Nickname: hamal
Hostname: hamal.guix.gnu.org
Port: 22
Physical: Yes
Vendor: SolidRun, HoneyComb LX2
Location: London, United Kingdom
Contact: Christopher Baines <mail@cbaines.net>
Cores: 16
SystemType: aarch64-linux
BaseSystem: Guix System

Nickname: dover
Hostname: dover.guix.gnu.org
Physical: Yes
Vendor: SoftIron, OverDrive 1000
Location: London, United Kingdom
Contact: Christopher Baines <mail@cbaines.net>
Cores: 4
SystemType: aarch64-linux
BaseSystem: Guix System

Nickname: lieserl
Hostname: 10.0.0.14
Physical: Yes
Vendor: SoftIron, OverDrive 1000
Location: Toulouse, France
Contact: Julien Lepiller <julien@lepiller.eu>
Cores: 4
SystemType: aarch64-linux
BaseSystem: Guix System
