;; Configuration for the machine hosting data.qa.guix.gnu.org,
;; git.guix-patches.cbaines.net and patches.guix-patches.cbaines.net
;;
;; Currently this is deployed on a Hetzner VM hosted in
;; Falkenstein. It has 16 cores, 32GB of RAM and 360GB root storage
;; with 200GB of attached storage
;;
;; Copyright © 2023 Christopher Baines <mail@cbaines.net>
;; Released under the GNU GPLv3 or any later version.

(use-modules (srfi srfi-1)
             (ice-9 match)
             (gnu)
             (guix gexp)
             (guix utils)
             (guix build-system gnu)
             (guix build utils)
             (guix git-download)
             (guix packages))
(use-service-modules networking ssh web certbot databases vpn
                     mail mcron getmail guix monitoring ci
                     version-control cgit linux)
(use-package-modules admin certs databases package-management
                     patchutils ci web version-control guile pkg-config autotools
                     guile-xyz python-web gnupg django sqlite)

(define patchwork-nginx-server-configuration
  (nginx-server-configuration
   (server-name '("patches.guix-patches.cbaines.net"))
   (listen '("443 ssl"))
   (root "/srv/http/patches.guix-patches.cbaines.net")
   (ssl-certificate "/etc/letsencrypt/live/patches.guix-patches.cbaines.net/fullchain.pem")
   (ssl-certificate-key "/etc/letsencrypt/live/patches.guix-patches.cbaines.net/privkey.pem")
   (locations
    (list
     (nginx-location-configuration
      (uri "/register")
      (body '("return 200 'Registration disabled due to spam, please email mail@cbaines.net\n';"
              "add_header Content-Type text/plain;")))
     (nginx-location-configuration
      (uri "/")
      (body '("proxy_pass http://patchwork-proxy;
               proxy_set_header Host            $host;
               proxy_set_header X-Forwarded-For $remote_addr;")))))))

(define guix-data-service-nginx-server-configuration
  (nginx-server-configuration
   (server-name '("data.qa.guix.gnu.org"))
   (listen '("443 ssl"))
   (root "/srv/http/data.qa.guix.gnu.org")
   (ssl-certificate "/etc/letsencrypt/live/data.qa.guix.gnu.org/fullchain.pem")
   (ssl-certificate-key "/etc/letsencrypt/live/data.qa.guix.gnu.org/privkey.pem")
   (locations
    (list
     (nginx-location-configuration
      (uri "/")
      (body '("try_files $uri $uri/ @guix-data-service;")))
     (nginx-location-configuration
      (uri "~ /dumps/(.+)")
      (body `(("root /var/lib/guix-data-service;"
               "try_files $uri =404;"))))
     (nginx-named-location-configuration
      (name "guix-data-service")
      (body '("proxy_pass http://guix-data-service-proxy;"
              "proxy_read_timeout 400s;"
              "proxy_set_header Host            $host;"
              "proxy_set_header X-Forwarded-For $remote_addr;"

              "proxy_cache guix-data-service;"
              "proxy_cache_revalidate on;"
              "proxy_cache_min_uses 3;"
              "proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504;"
              "proxy_cache_background_update on;"
              "proxy_cache_lock on;"
              "add_header X-Cache-Status $upstream_cache_status;"

              "if ($http_user_agent ~ (Bytespider|ClaudeBot) ) {
  return 403;
}"

              "gzip on;"
              "gzip_types text/html application/json;"
              "gzip_proxied any;")))))))

(define my-nginx-service-extra-content "
    types {
      text/plain run;
    }

    proxy_cache_path /var/cache/nginx/guix-data-service
                     levels=2
                     inactive=2d
                     keys_zone=guix-data-service:4m  # ~32K keys
                     max_size=1g
                     use_temp_path=off;")

(define my-nginx-service
  (service nginx-service-type
           (nginx-configuration
            (nginx
             (package
               (inherit nginx)
               (arguments
                (append
                 '(#:configure-flags '("--with-http_gzip_static_module"
                                       "--with-http_gunzip_module"))
                 (package-arguments nginx)))))
            (server-names-hash-bucket-size 64)
            (extra-content my-nginx-service-extra-content)
            (upstream-blocks
             (list
              (nginx-upstream-configuration
               (name "patchwork-proxy")
               (servers '("localhost:8080")))
              (nginx-upstream-configuration
               (name "guix-data-service-proxy")
               (servers '("127.0.0.1:8765")))))
            (server-blocks
             (list
              (nginx-server-configuration
               (server-name '("patches.guix-patches.cbaines.net"))
               (listen '("80"))
               (locations
                (list
                 (nginx-location-configuration
                  (uri "/")
                  (body
                   '("return 301 https://patches.guix-patches.cbaines.net$request_uri;")))
                 (nginx-location-configuration
                  (uri "^~ /.well-known/acme-challenge/")
                  (body '("root /srv/http/beid.cbaines.net;")))
                 (nginx-location-configuration
                  (uri "= /.well-known/acme-challenge/")
                  (body '("return 404;"))))))
              patchwork-nginx-server-configuration

              guix-data-service-nginx-server-configuration
              (nginx-server-configuration
               (inherit guix-data-service-nginx-server-configuration)
               (listen '("80"))
               (ssl-certificate #f)
               (ssl-certificate-key #f)
               (locations
                (append
                 (nginx-server-configuration-locations
                  guix-data-service-nginx-server-configuration)
                 (list
                  (nginx-location-configuration
                   (uri "^~ /.well-known/acme-challenge/")
                   (body '("root /srv/http/beid.cbaines.net;")))
                  (nginx-location-configuration
                   (uri "= /.well-known/acme-challenge/")
                   (body '("return 404;"))))))))))))

(define (guix-data-service-guix-cleanup guix-data-service)
  (program-file
   "guix-cleanup"
   (with-extensions
       (cons guix-data-service
             (map second (package-transitive-propagated-inputs
                          guix-data-service)))
     #~(begin
         (setvbuf (current-output-port) 'line)
         (setvbuf (current-error-port) 'line)

         (simple-format #t "~A: start: guix-cleanup\n"
                        (strftime "%c" (localtime (current-time))))

         (use-modules (prometheus)
                      ((guix scripts processes) #:select (daemon-sessions))
                      (guix build utils)
                      (guix build syscalls))

         (with-file-lock/no-wait "/tmp/guix-cleanup-lock"
           (lambda ()
             (simple-format #t "~A: skipping: guix-cleanup\n"
                            (strftime "%c" (localtime (current-time)))))
           (let* ((daemon-idle? (null? (daemon-sessions)))
                  (free-space-threshold (* 35 (expt 2 30)))
                  (free-space (free-disk-space "/gnu/store"))
                  (daemon-db-size
                   (stat:size (stat "/var/guix/db/db.sqlite")))
                  (low-free-space?
                   (< free-space free-space-threshold))
                  (large-daemon-db?
                   (> daemon-db-size
                      (* 13
                         (expt 2 30)))))
             (simple-format #t "guix-cleanup: free-space: ~A, daemon-db-size: ~A\n"
                            free-space
                            daemon-db-size)
             (when low-free-space?
               (invoke "guix" "gc"))
             (when large-daemon-db?
               (let loop ((attempt 0))
                 (with-exception-handler
                     (lambda (exn)
                       (if (< attempt 30)
                           (begin
                             (sleep 20)
                             (loop (+ 1 attempt)))
                           (simple-format #t "guix-cleanup: giving up vacuuming database\n")))
                   (lambda ()
                     (invoke "guix" "gc" "--vacuum-database"))
                   #:unwind? #t)))

             ;; Just record if something happened
             (when (or low-free-space?
                       large-daemon-db?)
               (let* ((registry (make-metrics-registry
                                 #:namespace "guixdataservicecleanup"))
                      (completion-time-metric
                       (make-gauge-metric registry "guix_cleanup_completion_time")))
                 (metric-set completion-time-metric (current-time))
                 (write-textfile registry
                                 (string-append
                                  "/var/lib/prometheus/node-exporter/"
                                  "guix-data-service-guix-cleanup.prom"))))))))))

(define (guix-data-service-derivation-cleanup guix-data-service)
  (program-file
   "guix-data-service-derivation-cleanup"
   (with-extensions
       (cons guix-data-service
             (map second (package-transitive-propagated-inputs
                          guix-data-service)))
     #~(begin
         (setvbuf (current-output-port) 'line)
         (setvbuf (current-error-port) 'line)

         (simple-format #t "~A: start: guix-data-service-derivation-cleanup\n"
                        (strftime "%c" (localtime (current-time))))
         (use-modules (prometheus)
                      (guix-data-service data-deletion))
         (begin
           (delete-unreferenced-derivations)

           (let* ((registry (make-metrics-registry
                             #:namespace "guixdataservicecleanup"))
                  (completion-time-metric
                   (make-gauge-metric registry "derivation_completion_time")))
             (metric-set completion-time-metric (current-time))
             (write-textfile registry
                             (string-append
                              "/var/lib/prometheus/node-exporter/"
                              "guix-data-service-derivation-cleanup.prom"))))))
   #:guile
   (car
    (assoc-ref (package-native-inputs guix-data-service)
               "guile"))))

(define (guix-data-service-nars-cleanup guix-data-service)
  (program-file
   "guix-data-service-nars-cleanup"
   (with-extensions
       (cons guix-data-service
             (map second (package-transitive-propagated-inputs
                          guix-data-service)))
     #~(begin
         (setvbuf (current-output-port) 'line)
         (setvbuf (current-error-port) 'line)

         (simple-format #t "~A: start: guix-data-service-nars-cleanup\n"
                        (strftime "%c" (localtime (current-time))))
         (use-modules (prometheus)
                      (guix-data-service data-deletion))
         (begin
           (delete-nars-for-unknown-store-paths)

           (let* ((registry (make-metrics-registry
                             #:namespace "guixdataservicecleanup"))
                  (completion-time-metric
                   (make-gauge-metric registry "nars_completion_time")))
             (metric-set completion-time-metric (current-time))
             (write-textfile registry
                             (string-append
                              "/var/lib/prometheus/node-exporter/"
                              "guix-data-service-nars-cleanup.prom"))))))
   #:guile
   (car
    (assoc-ref (package-native-inputs guix-data-service)
               "guile"))))

(define (guix-data-service-branch-cleanup guix-data-service)
  (program-file
   "guix-data-service-branch-cleanup"
   (with-extensions
       (cons guix-data-service
             (map second (package-transitive-propagated-inputs
                          guix-data-service)))
     #~(begin
         (setvbuf (current-output-port) 'line)
         (setvbuf (current-error-port) 'line)

         (simple-format #t "~A: start: guix-data-service-branch-cleanup\n"
                        (strftime "%c" (localtime (current-time))))
         (use-modules (squee)
                      (ice-9 match)
                      (prometheus)
                      (guix-data-service database)
                      (guix-data-service data-deletion)
                      (guix-data-service model package-derivation-by-guix-revision-range))
         (begin
           (define delete-revisions-from-branch
             (@@ (guix-data-service data-deletion) delete-revisions-from-branch))

           (delete-data-for-all-deleted-branches)
           (with-exception-handler
               (lambda (exn)
                 (simple-format
                  #t "failed deleting revisions except most recent: ~A\n"
                  exn))
             (lambda ()
               (delete-revisions-for-all-branches-except-most-recent-n 200))
             #:unwind? #t)

           (with-postgresql-connection
            "data-deletion"
            (lambda (conn)
              (for-each
               (match-lambda
                 ((git-repository-id branch-name)
                  ;; Temporary cleanup
                  (when (or (string-prefix? "wip-" branch-name)
                            (string-prefix? "version-" branch-name))
                    (simple-format #t "deleting ~A\n" branch-name)
                    (delete-data-for-branch conn
                                            (string->number git-repository-id)
                                            branch-name))

                  ;; We want to delete all revisions prior to the
                  ;; latest processed revision
                  (let ((commits
                         (map
                          car
                          (exec-query
                           conn
                           "
SELECT commit
FROM git_commits
INNER JOIN git_branches
  ON git_branches.id = git_commits.git_branch_id
 AND git_branches.git_repository_id = $1
 AND git_branches.name = $2
WHERE git_commits.datetime < (
  SELECT datetime
  FROM git_commits
  INNER JOIN git_branches
    ON git_branches.id = git_commits.git_branch_id
   AND git_branches.git_repository_id = $1
   AND git_branches.name = $2
  INNER JOIN load_new_guix_revision_jobs
    ON load_new_guix_revision_jobs.commit = git_commits.commit
   AND load_new_guix_revision_jobs.git_repository_id = $1
  ORDER BY CASE WHEN load_new_guix_revision_jobs.succeeded_at IS NULL THEN 0
           ELSE 1
           END DESC,
           datetime DESC
  LIMIT 1
)
  AND commit != ''
ORDER BY datetime DESC"
                           (list git-repository-id
                                 branch-name)))))
                    (unless (null? commits)
                      (simple-format
                       #t
                       "deleting ~A commits from ~A\n"
                       (length commits)
                       branch-name)
                      (delete-revisions-from-branch
                       conn
                       (string->number git-repository-id)
                       branch-name
                       ;; Delete the "branch deleted" commits as well
                       ;; TODO Handle this better
                       (cons "" commits))

                      (simple-format
                       #t
                       "repopulating package_derivations_by_guix_revision_range\n")
                      (insert-guix-revision-package-derivation-entries
                       conn
                       git-repository-id
                       branch-name)))))
               (exec-query
                conn
                "
SELECT git_repository_id, name
FROM git_branches
WHERE
    (git_repository_id = 1 AND name LIKE 'issue-%')
  OR
    (git_repository_id = 2 AND name NOT LIKE 'master')
ORDER BY id ASC"))))

           (let* ((registry (make-metrics-registry
                             #:namespace "guixdataservicecleanup"))
                  (completion-time-metric
                   (make-gauge-metric registry "branch_completion_time")))
             (metric-set completion-time-metric (current-time))
             (write-textfile registry
                             (string-append
                              "/var/lib/prometheus/node-exporter/"
                              "guix-data-service-branch-cleanup.prom"))))))
   #:guile
   (car
    (assoc-ref (package-native-inputs guix-data-service)
               "guile"))))

(define mcron-service-configuration
  (mcron-configuration
   (jobs (list #~(job "0 * * * *"
                      #$(guix-data-service-guix-cleanup my-guix-data-service))
               #~(job "2 * * * *"
                      #$(file-append
                         sqlite
                         "/bin/sqlite3 /var/guix/db/db.sqlite \"PRAGMA wal_checkpoint(TRUNCATE);\""))
               #~(job "0 1 * * 1"
                      "rm -r /var/lib/guix-data-service/.cache/guix/substitute/*")
               #~(job "0 1 * * 1"
                      "rm -r /var/log/postgresql/postgres*")
               #~(job "0 0 * * *"
                      #$(guix-data-service-branch-cleanup my-guix-data-service))
               #~(job "0 8 * * *"
                      #$(guix-data-service-branch-cleanup my-guix-data-service))
               #~(job "0 16 * * *"
                      #$(guix-data-service-branch-cleanup my-guix-data-service))
               #~(job "0 1 * * *"
                      #$(guix-data-service-nars-cleanup my-guix-data-service))
               #~(job "0 2 * * 0"
                      #$(guix-data-service-derivation-cleanup my-guix-data-service))))))

(operating-system
  (host-name "beid")
  (timezone "Europe/London")
  (locale "en_GB.utf8")

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets '("/dev/sda"))))

  (initrd-modules (append (list "virtio_scsi")
                          %base-initrd-modules))

  (file-systems (cons* (file-system
                         (device (file-system-label "root"))
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                        (device "/dev/disk/by-id/scsi-0HC_Volume_101385303")
                        (mount-point "/mnt/beid-postgresql-additional-data")
                        (type "ext4")
                        (options "discard"))
                       %base-file-systems))

  (swap-devices
   (list (swap-space
          (target "/swapfile"))))

  (users (cons (user-account
                (name "chris")
                (group "users")

                (supplementary-groups '("wheel"))
                (home-directory "/home/chris"))
               %base-user-accounts))

  (packages (cons* python-git-multimail
                   %base-packages))

  (services (cons*
             (service dhcp-client-service-type)

             (extra-special-file "/usr/bin/guix-data-service-branch-cleanup"
                                 (guix-data-service-branch-cleanup my-guix-data-service))
             (extra-special-file "/usr/bin/guix-data-service-nars-cleanup"
                                 (guix-data-service-nars-cleanup my-guix-data-service))
             (extra-special-file "/usr/bin/guix-data-service-derivation-cleanup"
                                 (guix-data-service-derivation-cleanup my-guix-data-service))
             (extra-special-file "/usr/bin/guix-data-service-guix-cleanup"
                                 (guix-data-service-guix-cleanup my-guix-data-service))

             (service ntp-service-type)
             (service openssh-service-type
                      (openssh-configuration
                       (permit-root-login 'prohibit-password)
                       (password-authentication? #f)))
             (service certbot-service-type
                      (certbot-configuration
                       (certificates
                        (list (certificate-configuration
                               (domains
                                '(;; Domains for the guix patches testing setup
                                  "patches.guix-patches.cbaines.net")))
                              (certificate-configuration
                               (domains
                                '("data.qa.guix.gnu.org")))))
                       (email "mail@cbaines.net")
                       (webroot "/srv/http/beid.cbaines.net")))
             my-nginx-service

             (service guix-data-service-type
                      (guix-data-service-configuration
                       (package my-guix-data-service)
                       (extra-options
                        '("--postgresql-statement-timeout=120000"
                          "--postgresql-connections=72"))
                       (extra-process-jobs-options
                        `("--max-processes=1"
                          "--latest-branch-revision-max-processes=1"
                          "--per-job-parallelism=6"
                          "--inferior-set-environment-variable=GUIX_DOWNLOAD_METHODS=upstream"
                          "--skip-system-tests"
                          ,(simple-format #f "--free-space-requirement=~A"
                                          (* 40 (expt 2 30)))))))

             (service prometheus-node-exporter-service-type)
             (service httpd-service-type
                      (httpd-configuration
                       (config
                        (httpd-config-file
                         (listen '("8080"))))))
             (service mcron-service-type
                      mcron-service-configuration)
             (service postgresql-service-type
                      (postgresql-configuration
                       (postgresql postgresql-13)
                       (config-file
                        (postgresql-config-file
                         (log-destination "stderr")
                         (hba-file
                          (plain-file "pg_hba.conf"
                                      "
local	all	all			trust
host	all	all	127.0.0.1/32 	md5
host	all	all	::1/128 	md5"))
                         (extra-config
                          '(("session_preload_libraries" "auto_explain")
                            ("auto_explain.log_min_duration" "30000ms")
                            ("log_autovacuum_min_duration" "30s")
                            ("max_connections" 500)
                            ("random_page_cost" 1.0)
                            ("work_mem" "1GB")
                            ("shared_buffers" "1GB")
                            ("effective_cache_size" "28 GB")
                            ("max_worker_processes" 4)
                            ("max_parallel_workers_per_gather" 4)
                            ("vacuum_cost_limit" 10000)
                            ("autovacuum_max_workers" 6)
                            ("autovacuum_vacuum_scale_factor" 0.02)
                            ("autovacuum_vacuum_cost_delay" 0)
                            ("max_parallel_maintenance_workers" 4)
                            ;; ("autovacuum_vacuum_threshold" 0)
                            ("default_statistics_target" 10000)
                            ("effective_io_concurrency" 200)
                            ("logging_collector" #t)
                            ("log_directory" "/var/log/postgresql")))))))

             (service earlyoom-service-type
                      (earlyoom-configuration
                       (minimum-available-memory 3)
                       (minimum-free-swap 5)))

             (service patchwork-service-type
                      (patchwork-configuration
                       (patchwork
                        (package
                         (inherit patchwork)
                         (source
                          (origin
                            (method git-fetch)
                            (uri (git-reference
                                  (url "https://git.cbaines.net/git/patchwork")
                                  (commit "add-captcha")))
                            (sha256
                             (base32
                              "130mnj1az1f543sh8ph42kdqh032zzmm5ymd1ap0yyl90122xrpb"))))
                         (propagated-inputs
                          `(,@(package-propagated-inputs patchwork)
                            ("python-django-simple-math-captcha"
                             ,python-django-simple-math-captcha)))))
                       (settings-module
                        (patchwork-settings-module
                         (allowed-hosts '("patches.guix-patches.cbaines.net"))
                         (default-from-email "patchwork@cbaines.net")
                         (admins
                          '(("Christopher Baines" "mail@cbaines.net")))
                         (extra-settings
                          "
SESSION_COOKIE_DOMAIN = '.guix-patches.cbaines.net'

#REST_RESULTS_PER_PAGE=500
MAX_REST_RESULTS_PER_PAGE=1000
FORCE_HTTPS_LINKS=True

EMAIL_HOST = 'smtp.cbaines.net'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'patchwork'
EMAIL_HOST_PASSWORD = open('/etc/getmail-patchwork-imap-password').readline().rstrip()
EMAIL_USE_TLS = True

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

")))
                       (domain "patches.guix-patches.cbaines.net")
                       (getmail-retriever-config
                        (getmail-retriever-configuration
                         (type "SimpleIMAPSSLRetriever")
                         (server "imap.cbaines.net")
                         (port 993)
                         (username "patchwork")
                         (password-command
                          (list (file-append coreutils "/bin/cat")
                                "/etc/getmail-patchwork-imap-password"))
                         (extra-parameters
                          '((mailboxes . ("Patches"))))))))
             (modify-services
                 %base-services
               (guix-service-type
                config => (guix-configuration
                           (inherit config)
                           (substitute-urls
                            `(,@(guix-configuration-substitute-urls config)
                              ;; So that the data service can
                              ;; substitute from itself
                              "https://data.qa.guix.gnu.org"))
                           (authorized-keys
                            (list
                             (local-file "keys/guix/bordeaux.guix.gnu.org-export.pub")
                             (local-file "keys/guix/berlin.guixsd.org-export.pub")
                             (local-file "keys/guix/data.qa.guix.gnu.org.pub")))
                           (max-silent-time (* 60 60 3))
                           (timeout (* 60 60 24))
                           (build-accounts 32)
                           (extra-options '("--max-jobs=4"))))))))
