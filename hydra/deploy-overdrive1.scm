(use-modules (sysadmin overdrive))

(list
 (machine
  (operating-system (overdrive-system "overdrive1"
                                      #:wireguard-ip "10.0.0.3/32"))
  (environment managed-host-environment-type)
  (configuration
   (machine-ssh-configuration
    (host-name "overdrive1.guix.gnu.org")
    (port 52522)
    (user (getenv "USER"))
    (build-locally? #t)
    (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPf2f93c90oi9s9qGVGWC3sDgG7kEBvIEwR021NsfG+z")
    (system "aarch64-linux")))))
