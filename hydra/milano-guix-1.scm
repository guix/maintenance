;; Guix System configuration for milano-guix-1.unimi.it build machine
;; HP ProLiantDL380p Gen8
;; Copyright © 2019 Giovanni Biscuolo <g@xelera.eu>
;; Released under the GNU GPLv3 or any later version.

;; DNS name in UNIMI data centre: milano-guix-1.mips.di.unimi.it.

;; SHA256:NMG2qnisy+qm8KXyTAg5kcyzkghmnYiWiM+Eyt83PcY.

(use-modules (gnu)
             (guix gexp))
(use-service-modules databases getmail guix networking mcron ssh
                     virtualization web monitoring linux)
(use-package-modules certs screen tmux ssh linux package-management)

;; The following definition are **almost** copied from dover.scm: a
;; quick hack to have a(n almost) self contained config file

;; FIXME: refactor using modules (e.g. like berlin.scm)

(define (sysadmin name full-name)
  (user-account
   (name name)
   (comment full-name)
   (group "users")
   (supplementary-groups '("wheel" "kvm"))
   (home-directory (string-append "/home/" name))))

(define %accounts
  (list (sysadmin "ludo" "Ludovic Courtès")
        (sysadmin "rekado" "Ricardo Wurmus")
        (sysadmin "andreas" "Andreas Enge")
        (sysadmin "cbaines" "Christopher Baines")
        (sysadmin "degrees380" "Giovanni Biscuolo")
        (sysadmin "atrent" "Andrea Trentini")
        (sysadmin "janneke" "Jan Nieuwenhuizen")))

(define gc-job
  ;; Run 'guix gc' at 3AM every day.
  #~(job '(next-hour '(3)) "guix gc -F 350G"))

(define full-gc-job
  #~(job '(next-hour-from (next-day (range 1 31 7)) '(2))
         (string-append #$guix "/bin/guix gc")))

(define btrfs-balance-job
  ;; Re-allocate chunks which are using less than 5% of their chunk
  ;; space, to regain Btrfs 'unallocated' space.  The usage is kept
  ;; low (5%) to minimize wear on the SSD.  Runs at 5 AM every 3 days.
  #~(job '(next-hour-from (next-day (range 1 31 3)) '(5))
         (lambda ()
           (system* #$(file-append btrfs-progs "/bin/btrfs")
                    "balance" "start" "-dusage=5" "/"))
         "btrfs-balance"))


(define (childhurd-os id)
  (define childhurd-name
    (format #f "milano-childhurd-~2,'0d" id))

  (operating-system
    (inherit %hurd-vm-operating-system)
    (host-name childhurd-name)
    (services
     (cons*
      (service guix-build-coordinator-agent-service-type
               (guix-build-coordinator-agent-configuration
                (coordinator "https://coordinator.bordeaux.guix.gnu.org")
                (authentication
                 (guix-build-coordinator-agent-dynamic-auth-with-file
                  (agent-name childhurd-name)
                  (token-file
                   "/etc/guix-build-coordinator-agent-token")))
                (max-parallel-builds 1)
                (max-allocated-builds 1)
                (derivation-substitute-urls
                 (list "https://data.guix.gnu.org"
                       "https://data.qa.guix.gnu.org"))
                (non-derivation-substitute-urls
                 (list "https://bordeaux.guix.gnu.org"))))

      (modify-services (operating-system-user-services
                        %hurd-vm-operating-system)
        (guix-service-type
         config => (guix-configuration
                    (inherit config)
                    (substitute-urls
                     '("https://bordeaux.guix.gnu.org"))

                    (max-silent-time (* 12 3600))
                    (timeout (* 48 3600))

                    (authorized-keys
                     (list
                      (local-file "keys/guix/bordeaux.guix.gnu.org-export.pub")
                      (local-file "keys/guix/data.guix.gnu.org.pub")
                      (local-file "keys/guix/data.qa.guix.gnu.org.pub"))))))))))

;; The actual machine

(operating-system
  (locale "en_US.utf8")
  (timezone "Europe/Rome")
  (keyboard-layout
   (keyboard-layout "us" "winkeys"))

  (kernel-arguments '("vga=791"))

  (initrd-modules (append (list "hpsa") ;; Smart Array P420i controller
                          %base-initrd-modules))

  (bootloader
   (bootloader-configuration
    (bootloader grub-bootloader)
    (targets (list "/dev/sda" "/dev/sdd"))
    (keyboard-layout keyboard-layout)))

  (file-systems
   (cons* (file-system
            (mount-point "/")
            (device
             (uuid "bb792593-1498-4bcf-b4df-84c9cba01bad"))
            (type "btrfs"))
          %base-file-systems))

  (swap-devices
   (list (swap-space
          (target (uuid "5cfe4b49-8c65-493b-9a7d-61ca2342208d")))
         (swap-space
          (target (uuid "cb7def43-1931-48b8-8fd8-dd2b6837094e")))))

  (host-name "milano-guix-1")

  (users (append %accounts %base-user-accounts))

  (packages (cons* btrfs-progs screen tmux openssh strace
                   %base-packages))

  ;; Services
  (services
   (append (list
            (service dhcp-client-service-type
                     (dhcp-client-configuration
                      (interfaces '("eno1"))))

            (service openssh-service-type
                     (openssh-configuration
                      (port-number 22)
                      (password-authentication? #f)
                      (extra-content "ListenAddress 0.0.0.0")
                      (authorized-keys
                       `(("degrees380" ,(local-file "keys/ssh/degrees380.pub"))
                         ("bayfront" ,(local-file "keys/ssh/bayfront.pub"))
                         ("atrent" ,(local-file "keys/ssh/atrent.pub"))
                         ("ludo" ,(local-file "keys/ssh/ludo.pub"))
                         ("andreas" ,(local-file "keys/ssh/andreas.pub"))
                         ("cbaines" ,(local-file "keys/ssh/cbaines.pub"))
                         ("rekado" ,(local-file "keys/ssh/rekado.pub"))))))

            (service ntp-service-type)

            (service prometheus-node-exporter-service-type)

            (service earlyoom-service-type)

            ;; The Prometheus node exporter isn't reachable on the
            ;; 9100 port, so use NGinx to proxy the requests
            (service
             nginx-service-type
             (nginx-configuration
              (upstream-blocks
               (list (nginx-upstream-configuration
                      (name "prometheus-node-exporter")
                      (servers '("localhost:9100")))))
              (server-blocks
               (list
                (nginx-server-configuration
                 (server-name '("milano-guix-1.mips.di.unimi.it"))
                 (listen '("80" "[::]:80"))
                 (locations
                  (list
                   (nginx-location-configuration
                    (uri "/")
                    (body '("proxy_pass http://prometheus-node-exporter;"))))))))))

            (service mcron-service-type
                     (mcron-configuration
                      (jobs (list gc-job
                                  full-gc-job
                                  btrfs-balance-job))))

           (service guix-build-coordinator-agent-service-type
                    (guix-build-coordinator-agent-configuration
                     (coordinator "https://coordinator.bordeaux.guix.gnu.org")
                     (authentication
                      (guix-build-coordinator-agent-password-file-auth
                       (uuid "b57898c3-7187-4c56-9d3c-4e68b954b79a")
                       (password-file
                        "/etc/guix-build-coordinator-agent-password")))
                     (max-parallel-builds 8)
                     (max-1min-load-average 20)
                     (systems '("x86_64-linux"))
                     (derivation-substitute-urls
                      (list "https://data.guix.gnu.org"
                            "https://data.qa.guix.gnu.org"))
                     (non-derivation-substitute-urls
                      (list "https://bordeaux.guix.gnu.org")))))

           (let ((childhurd-count 2))
             (map (lambda (id)
                    (service hurd-vm-service-type
                             (hurd-vm-configuration
                              (id id)
                              (os (childhurd-os id))
                              (disk-size (* 20000 (expt 2 20))) ; 20G
                              (memory-size 4096))))
                  (iota childhurd-count 1)))

           (modify-services %base-services
             (guix-service-type
              config => (guix-configuration
                         (inherit config)
                         (substitute-urls
                          '("https://bordeaux.guix.gnu.org"))

                         (build-accounts 64)

                         (max-silent-time (* 24 3600))
                         (timeout (* 72 3600))

                         (extra-options '("--cores=16"))

                         (authorized-keys
                          (list
                           (local-file "keys/guix/bordeaux.guix.gnu.org-export.pub")
                           (local-file "keys/guix/data.guix.gnu.org.pub")
                           (local-file "keys/guix/data.qa.guix.gnu.org.pub")))))))))
