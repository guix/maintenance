# CloudWatch

# For the month of July, 2019, this threshold will give us 1 or 2 days
# to react before we exceed the overall agreed-upon budget.
resource "aws_cloudwatch_metric_alarm" "alarm-estimated-charges-340-usd" {
  alarm_name = "alarm-estimated-charges-340-usd"
  alarm_description = "Estimated charges have exceeded 340 USD"
  namespace = "AWS/Billing"
  metric_name = "EstimatedCharges"
  statistic = "Maximum"
  period = "21600" # 6 hours
  evaluation_periods = "1"
  comparison_operator = "GreaterThanThreshold"
  threshold = "300"
  actions_enabled = true
  alarm_actions = ["${aws_sns_topic.guix-billing-alarms.arn}"]
  dimensions {
    Currency = "USD"
  }
}
