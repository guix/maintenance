# This file defines our backend and provider.  The remaining resources
# are defined in separate *.tf files in this directory.  Terraform
# merges them all together when it runs, and the order doesn't matter.
# See: https://www.terraform.io/docs/configuration/index.html

# Backend documentation:
# https://www.terraform.io/docs/backends/types/s3.html
terraform {
  # This backend will not work if you delete the corresponding bucket.
  backend "s3" {
    bucket = "guix-terraform-state"
    key    = "state"
    # The backend documentation does not clarify if it respects the
    # region specified in the provider configuration, so we explicitly
    # set the region here, too.  To understand why the region must be
    # us-east-1, see the coments in the provider configuration.
    region = "us-east-1"
    # Locking will not work if you delete the corresponding table.
    dynamodb_table = "terraform-locking"
  }
}

# Provider documentation: https://www.terraform.io/docs/providers/aws
#
# Some AWS credentials documentation:
# https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html
#
# You must set up an AWS Credentials file to use the AWS provider.
# For example, you might put the following in ~/.aws/credentials:
#
#   [guix]
#   aws_access_key_id=ACCESS_KEY_ID
#   aws_secret_access_key=SECRET_ACCESS_KEY
#
# Then you can invoke "terraform" like this to use the credentials:
#
#   AWS_PROFILE=guix terraform apply
#
# The environment variable AWS_PROFILE tells the AWS provider to look
# for a "profile" named "guix".  You can use any profile name you
# want, but it needs to match the profile name in the AWS credentials
# file.

provider "aws" {
  # We must use us-east-1 because that's where our certificate must
  # exist.  See the comments near the definition of
  # berlin-mirror-certificate for details.
  region = "us-east-1"
}
